package com.models.enums;

public enum TaskType {
    BUG,
    STORY,
    FEEDBACK
}
