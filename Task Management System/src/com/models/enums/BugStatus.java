package com.models.enums;

public enum BugStatus {
    ACTIVE,
    FIXED
}
