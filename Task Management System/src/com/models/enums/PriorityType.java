package com.models.enums;

public enum PriorityType {
    HIGH,
    MEDIUM,
    LOW
}
