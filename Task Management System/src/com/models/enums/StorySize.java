package com.models.enums;

public enum StorySize {
    LARGE,
    MEDIUM,
    SMALL
}
