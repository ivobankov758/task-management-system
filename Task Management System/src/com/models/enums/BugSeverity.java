package com.models.enums;

public enum BugSeverity {
    CRITICAL,
    MAJOR,
    MINOR
}
