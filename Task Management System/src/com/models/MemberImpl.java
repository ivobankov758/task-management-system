package com.models;

import com.models.contracts.Assignable;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MemberImpl implements Member {
    public final static int MEMBER_NAME_MIN_LEN = 5;
    public final static int MEMBER_NAME_MAX_LEN = 15;
    private final static String MEMBER_NAME_Error =
            String.format("Member name length should be between %d and %d symbols",
                    MEMBER_NAME_MIN_LEN, MEMBER_NAME_MAX_LEN);


    private String memberName;
    private final List<Assignable> tasks;
    private final List<String> history;
    private Team team;


    public MemberImpl(String memberName) {
        setMemberName(memberName);

        this.tasks = new ArrayList<>();
        this.history = new ArrayList<>();

    }

    //---Setters---

    private void setMemberName(String memberName) {
        ValidationHelpers.validateValueInRange
                (memberName.length(), MEMBER_NAME_MIN_LEN, MEMBER_NAME_MAX_LEN, MEMBER_NAME_Error);
        this.memberName = memberName;
    }

    public void setTeam(Team team){
        this.team=team;
    }

    //---Getters---
    @Override
    public String getName() {
        return memberName;
    }

    @Override
    public List<Assignable> getTasks() {
        return new ArrayList<>(tasks);
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    public Team getTeam() {
        return team;
    }
//---Methods---

    @Override
    public void addHistory(String description) {
        history.add(String.format("%s - %s", ParsingHelpers.formatTime(LocalDateTime.now()), description));
    }

    @Override
    public void addTask(Assignable task) {
        tasks.add(task);
    }

    @Override
    public void removeTask(Assignable taskToRemove) {
            tasks.remove(taskToRemove);
    }

    @Override
    public String toString() {
        return  "%nName: " + memberName+
                "%n---Tasks---%n" + tasks.toString() +
                "%n---History---%n" + history.toString() +
                "%nTeam: " + team.getName()+
                "%n";
    }
}
