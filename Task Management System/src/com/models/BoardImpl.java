package com.models;

import com.models.contracts.Board;
import com.models.contracts.Task;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    public static final int BOARD_NAME_MIN_LEN = 5;
    public static final int BOARD_NAME_MAX_LEN = 10;
    private static final String BOARD_NAME_ERROR = String.format(
            "The Board Name length cannot be less than %d or more than %d symbols long.",
            BOARD_NAME_MIN_LEN, BOARD_NAME_MAX_LEN);


    private String boardName;
    private final List<Task> tasks;
    private final List<String> history;

    public BoardImpl(String boardName) {
        setBoardName(boardName);
        this.tasks = new ArrayList<>();
        this.history = new ArrayList<>();
    }

    //---Setters---
    public void setBoardName(String boardName) {
        ValidationHelpers.validateValueInRange(boardName.length(), BOARD_NAME_MIN_LEN, BOARD_NAME_MAX_LEN, BOARD_NAME_ERROR);
        this.boardName = boardName;
    }

    //---Getters---
    @Override
    public String getName() {
        return boardName;
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }


    //---METHODS---
    @Override
    public void addTask(Task task) {
        tasks.add(task);
    }

    @Override
    public void addHistory(String description) {
        history.add(String.format("%s - %s", ParsingHelpers.formatTime(LocalDateTime.now()), description));
    }

    @Override
    public String toString() {
        return  "\nName:" + boardName +
                "\n---Tasks---\n" + tasks.toString()+
                "\n---History---\n" + history.toString()+
                "\n";
    }
}
