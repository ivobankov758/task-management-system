package com.models;

import com.models.contracts.Bug;
import com.models.contracts.Member;
import com.models.enums.BugSeverity;
import com.models.enums.BugStatus;
import com.models.enums.PriorityType;
import com.models.enums.TaskType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BugImpl extends TaskImpl implements Bug {
    public static final String INVALID_STEPS_SIZE = "Steps can't be empty!";
    public static final String INVALID_ASSIGNEE = "%s is already assigned to %s";
    public static final String LOG_SEVERITY = "The severity of item with ID %d was changed from %s to %s.";

    private PriorityType priority;
    private Member assignee;
    private List<String> steps;
    private BugSeverity severity;
    private BugStatus status;

    public BugImpl(int id, String title, String description, PriorityType priority,
                   BugSeverity severity, List<String> steps) {
        super(id, title, description);

        this.assignee = DEFAULT_ASSIGNEE;
        this.priority = priority;
        this.severity = severity;
        this.status = BugStatus.ACTIVE;
        setSteps(steps);
    }

    @Override
    public PriorityType getPriority() {
        return priority;
    }

    @Override
    public Member getAssignee() {
        return assignee;
    }

    @Override
    public List<String> getSteps() {
        return new ArrayList<>(steps);
    }

    @Override
    public BugSeverity getSeverity() {
        return severity;
    }

    @Override
    public BugStatus getStatus() {
        return status;
    }

    @Override
    public void changePriority(PriorityType priority) {
        if(this.priority == priority){
            throw new IllegalArgumentException(
                    String.format("%s's priority is already set as %s", getTitle(), getPriority()));
        }

        historyLogger(String.format(LOG_PRIORITY, getId(), getPriority(), priority));
        this.priority = priority;
    }

    @Override
    public void setAssignee(Member assignee) {
        if(this.assignee.equals(assignee)){
            throw new IllegalArgumentException(
                    String.format(INVALID_ASSIGNEE, getTitle(), getAssignee().getName()));
        }
        else {
            this.assignee = assignee;
            historyLogger(String.format(LOG_ASSIGNEE, getId(), getAssignee().getName()));
        }
    }

    @Override
    public void changeStatus(BugStatus status) {
        if(this.status == status){
            throw new IllegalArgumentException(
                    String.format("%s's status is already set as %s", getTitle(), getStatus()));
        }
        historyLogger(String.format(LOG_STATUS, getId(), getStatus(), status));
        this.status = status;
    }

    @Override
    public void changeSeverity(BugSeverity severity) {
        if(this.severity == severity){
            throw new IllegalArgumentException(
                    String.format("%s's severity is already set as %s", getTitle(), getSeverity()));
        }
        historyLogger(String.format(LOG_SEVERITY, getId(), getSeverity(), severity));
        this.severity = severity;
    }

    private void setSteps(List<String> steps) {
        if (steps.size() < 1){
            throw new IllegalArgumentException(INVALID_STEPS_SIZE);
        }
        this.steps = new ArrayList<>(steps);
    }

    @Override
    public TaskType getType() {
        return TaskType.BUG;
    }

    @Override
    public String toString() {
        return String.format("%s" +
                "Steps: %s%n", super.toString(), printSteps());
    }

    @Override
    public String printImportantInfo() {
        return String.format("Type: %s%n" +
                "%s" +
                "Priority: %s%n" +
                "Assignee: %s%n" +
                "Severity: %s%n" +
                "Status: %s%n",
                getType(), super.printImportantInfo(), getPriority(),
                getAssignee().getName(),getSeverity(), getStatus());
    }

    private String printSteps(){

        if(getSteps().isEmpty()){
            return "---NO STEPS---";
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(System.lineSeparator());

        String result = getSteps()
                .stream()
                .map((step -> String.format("%s%n", step)))
                .collect(Collectors.joining(""));

        stringBuilder.append(result);
        return stringBuilder.toString();
    }
}
