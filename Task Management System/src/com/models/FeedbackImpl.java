package com.models;

import com.models.contracts.Feedback;
import com.models.enums.FeedbackStatus;
import com.models.enums.TaskType;
import com.utils.ValidationHelpers;

public class FeedbackImpl extends TaskImpl implements Feedback {
    public static final String INVALID_RATING = "Rating can't be negative!";
    public static final String LOG_RATING = "The rating of item with ID %d was changed from %d to %d.";

    private int rating;
    private FeedbackStatus status;

    public FeedbackImpl(int id, String title, String description, int rating) {
        super(id, title, description);
        setRating(rating);
        this.status = FeedbackStatus.NEW;
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public FeedbackStatus getStatus() {
        return status;
    }

    @Override
    public void changeRating(int rating) {
        if (this.rating == rating) {
            throw new IllegalArgumentException(
                    String.format("%s's rating is already set at %s", getTitle(), getRating()));
        }
        int oldRating = getRating();
        setRating(rating);
        historyLogger(String.format(LOG_RATING, getId(), oldRating, getRating()));

    }

    @Override
    public void changeStatus(FeedbackStatus status) {
        if (this.status == status) {
            throw new IllegalArgumentException(
                    String.format("%s's status is already set as %s", getTitle(), getStatus()));
        }
        historyLogger(String.format(LOG_STATUS, getId(), getStatus(), status));
        this.status = status;
    }

    @Override
    public TaskType getType() {
        return TaskType.FEEDBACK;
    }

    @Override
    public String toString() {
        return String.format("%s",super.toString());
    }

    @Override
    public String printImportantInfo() {
        return String.format("Type: %s%n" +
                "%s" +
                "Rating: %d%n" +
                "Status: %s%n",
                getType(), super.printImportantInfo(), getRating(), getStatus());
    }

    private void setRating(int rating) {
        ValidationHelpers.validatePositive(rating, INVALID_RATING);
        this.rating = rating;
    }
}
