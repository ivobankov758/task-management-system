package com.models;

import com.models.contracts.Comment;
import com.models.contracts.Task;
import com.utils.ListingHelpers;
import com.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class TaskImpl implements Task {
    public static final int MIN_TITLE_LEN = 10;
    public static final int MAX_TITLE_LEN = 50;
    public static final int MIN_DESCRIPTION_LEN = 10;
    public static final int MAX_DESCRIPTION_LEN = 500;

    public static final String INVALID_TITLE =
            String.format("Title should be between %d and %d symbols!", MIN_TITLE_LEN, MAX_TITLE_LEN);

    public static final String INVALID_DESCRIPTION =
            String.format("Description should be between %d and %d symbols!", MIN_DESCRIPTION_LEN, MAX_DESCRIPTION_LEN);
    public static final String LOG_ASSIGNEE = "Task with ID %d was assigned to %s";
    public static final String LOG_PRIORITY = "The priority of item with ID %d was changed from %s to %s.";
    public static final String LOG_STATUS = "The status of item with ID %d was changed from %s to %s.";

    private final int id;
    private String title;
    private String description;
    private final List<Comment> comments;
    private final List<String> history;

    public TaskImpl(int id, String title, String description) {
        this.id = id;
        setTitle(title);
        setDescription(description);
        this.comments = new ArrayList<>();
        this.history = new ArrayList<>();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
        historyLogger(String.format("A comment was added to item with ID %d.", getId()));
    }

    @Override
    public void historyLogger (String log){
        history.add(log);
    }

    @Override
    public String toString() {

        return String.format("%s" +
                        "Description: %s%n" +
                        "Comments: %s%n" +
                        "History: %s",
                printImportantInfo(), getDescription(), printComments(), ListingHelpers.printHistory(getHistory()));
    }

    @Override
    public String printImportantInfo(){
        return String.format("Id: %d%n" +
                        "Title: %s%n", getId(), getTitle());
    }

    private String printComments(){

        if (getComments().isEmpty()){
            return "---NO COMMENTS---";
        }

        return getComments()
                .stream()
                .map((comment -> comment.toString()))
                .collect(Collectors.joining(""));
    }

    private void setTitle(String title) {
        ValidationHelpers.validateStringLength(title, MIN_TITLE_LEN, MAX_TITLE_LEN, INVALID_TITLE);
        this.title = title;
    }

    private void setDescription(String description) {
        ValidationHelpers.validateStringLength(description,MIN_DESCRIPTION_LEN, MAX_DESCRIPTION_LEN, INVALID_DESCRIPTION);
        this.description = description;
    }
}
