package com.models;

import com.models.contracts.Comment;
import com.utils.ValidationHelpers;

public class CommentImpl implements Comment {
    public static final int CONTENT_LEN_MIN = 3;
    public static final int CONTENT_LEN_MAX = 200;
    private static final String CONTENT_LEN_ERR = String.format(
            "Content must be between %d and %d characters long!",
            CONTENT_LEN_MIN,
            CONTENT_LEN_MAX);

    private String author;
    private String content;

    public CommentImpl(String author, String content) {
        setAuthor(author);
        setContent(content);
    }

    @Override
    public String getAuthor() {
        return author;
    }

    @Override
    public String getContent() {
        return content;
    }

    private void setAuthor(String author) {
        this.author = author;
    }

    private void setContent(String content) {
        ValidationHelpers.validateStringLength(content, CONTENT_LEN_MIN, CONTENT_LEN_MAX, CONTENT_LEN_ERR);
        this.content = content;
    }

    @Override
    public String toString() {

        return String.format("%n---COMMENT---%n" +
                "Author: %s%n" +
                "Content: %s%n" +
                "-------------", getAuthor(), getContent());
    }
}
