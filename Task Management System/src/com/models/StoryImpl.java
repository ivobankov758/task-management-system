package com.models;

import com.models.contracts.Member;
import com.models.contracts.Story;
import com.models.enums.PriorityType;
import com.models.enums.StorySize;
import com.models.enums.StoryStatus;
import com.models.enums.TaskType;

public class StoryImpl extends TaskImpl implements Story {

    public static final String LOG_SIZE = "The size of item with ID %d was changed from %s to %s.";

    private PriorityType priority;
    private Member assignee;
    private StorySize size;
    private StoryStatus status;

    public StoryImpl(int id, String title, String description, PriorityType priority, StorySize size) {
        super(id, title, description);
        this.assignee = DEFAULT_ASSIGNEE;
        this.priority = priority;
        this.size = size;
        this.status = StoryStatus.NOT_DONE;
    }

    @Override
    public PriorityType getPriority() {
        return priority;
    }

    @Override
    public Member getAssignee() {
        return assignee;
    }

    @Override
    public StorySize getSize() {
        return size;
    }

    @Override
    public StoryStatus getStatus() {
        return status;
    }

    @Override
    public void changePriority(PriorityType priority) {
        if(this.priority == priority){
            throw new IllegalArgumentException(
                    String.format("%s's priority is already set as %s", getTitle(), getPriority()));
        }

        historyLogger(String.format(LOG_PRIORITY, getId(), getPriority(), priority));
        this.priority = priority;
    }

    @Override
    public void setAssignee(Member assignee) {
        if(this.assignee.equals(assignee)){
            throw new IllegalArgumentException(
                    String.format("%s is already assigned to %s", getTitle(), getAssignee().getName()));
        }
        else {
            this.assignee = assignee;
            historyLogger(String.format(LOG_ASSIGNEE, getId(), getAssignee().getName()));
        }
    }

    @Override
    public void changeSize(StorySize size) {
        if(this.size == size){
            throw new IllegalArgumentException(
                    String.format("%s's size is already set as %s", getTitle(), getSize()));
        }
        historyLogger(String.format(LOG_SIZE, getId(), getSize(), size));
        this.size = size;
    }

    @Override
    public void changeStatus(StoryStatus status) {
        if(this.status == status){
            throw new IllegalArgumentException(
                    String.format("%s's status is already set as %s", getTitle(), getStatus()));
        }
        historyLogger(String.format(LOG_STATUS, getId(), getStatus(), status));
        this.status = status;
    }

    @Override
    public TaskType getType() {
        return TaskType.STORY;
    }

    @Override
    public String toString() {
        return String.format("%s", super.toString());
    }

    @Override
    public String printImportantInfo() {
        return String.format("Type: %s%n" +
                "%s" +
                "Priority: %s%n" +
                "Assignee: %s%n" +
                "Size: %s%n" +
                "Status: %s%n",
                getType(), super.printImportantInfo(), getPriority(),
                getAssignee().getName(), getSize(), getStatus());
    }
}
