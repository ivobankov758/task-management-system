package com.models;

import com.models.contracts.Board;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {
    public static final int TEAM_NAME_MIN_LEN = 5;
    public static final int TEAM_NAME_MAX_LEN = 15;
    private static final String TEAM_NAME_ERROR =
            String.format("Team Name length cannot be less than %d or more than %d symbols long.",
                    TEAM_NAME_MIN_LEN, TEAM_NAME_MAX_LEN);


    private String teamName;
    private final List<Member> members;
    private final List<Board> boards;
    private final List<String> history;

    public TeamImpl(String teamName) {

        setTeamName(teamName);
        this.members = new ArrayList<>();
        this.boards = new ArrayList<>();
        this.history = new ArrayList<>();
    }

    // ----Getters---
    @Override
    public String getName() {
        return teamName;
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(members);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public List<String> getHistory() {
        return new ArrayList<>(history);
    }

    //----Setters----
    public void setTeamName(String teamName) {
        ValidationHelpers.validateValueInRange(teamName.length(),
                TEAM_NAME_MIN_LEN, TEAM_NAME_MAX_LEN, TEAM_NAME_ERROR);

        this.teamName = teamName;
    }

    //---METHODS---
    @Override
    public void addBoard(Board board) {
        boards.add(board);

    }

    @Override
    public void addMember(Member member) {
        members.add(member);
    }

    @Override
    public void addHistory(String description) {
        history.add(String.format("%s - %s", ParsingHelpers.formatTime(LocalDateTime.now()),description));
    }

    @Override
    public String toString() {
        return "---Team---\n" +
                "\nName: " + teamName +
                "\n---Members---\n" + members.toString() +
                "\n---Boards---\n" + boards.toString();
    }
}
