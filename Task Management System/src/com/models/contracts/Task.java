package com.models.contracts;

import com.models.enums.PriorityType;
import com.models.enums.TaskType;

import java.util.List;

public interface Task extends Commentable, Identifiable{
    String getTitle();
    String getDescription();
    List<String> getHistory();
    TaskType getType();
    String printImportantInfo();
    void historyLogger (String log);
    void addComment(Comment comment);
}
