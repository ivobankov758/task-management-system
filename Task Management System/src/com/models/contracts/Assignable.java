package com.models.contracts;

import com.models.MemberImpl;
import com.models.enums.PriorityType;

public interface Assignable extends Task{
    Member DEFAULT_ASSIGNEE = new MemberImpl("unassigned");

    Member getAssignee();
    PriorityType getPriority();
    void changePriority(PriorityType priority);
    void setAssignee(Member assignee);
}
