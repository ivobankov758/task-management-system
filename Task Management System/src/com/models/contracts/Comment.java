package com.models.contracts;

public interface Comment {
    String getAuthor();
    String getContent();
}
