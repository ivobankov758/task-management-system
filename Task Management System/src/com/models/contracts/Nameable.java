package com.models.contracts;

public interface Nameable {
    String getName();
}
