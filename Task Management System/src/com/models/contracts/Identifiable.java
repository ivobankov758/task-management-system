package com.models.contracts;

public interface Identifiable{
    int getId();
}
