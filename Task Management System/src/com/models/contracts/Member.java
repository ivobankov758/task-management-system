package com.models.contracts;

import java.util.List;

public interface Member extends Nameable {
 List<Assignable> getTasks();
 List<String> getHistory();
 Team getTeam();
 void setTeam(Team team);
 void addHistory(String history);
 void addTask(Assignable task);
 void removeTask(Assignable task);

}
