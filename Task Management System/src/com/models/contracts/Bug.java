package com.models.contracts;

import com.models.enums.BugSeverity;
import com.models.enums.BugStatus;
import com.models.enums.PriorityType;

import java.util.List;

public interface Bug extends Assignable{
    List<String> getSteps();
    BugSeverity getSeverity();
    BugStatus getStatus();
    void changeSeverity(BugSeverity severity);
    void changeStatus(BugStatus status);
}
