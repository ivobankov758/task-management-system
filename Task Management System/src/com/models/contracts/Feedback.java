package com.models.contracts;

import com.models.enums.FeedbackStatus;

public interface Feedback extends Task{
    int getRating();
    FeedbackStatus getStatus();
    void changeRating(int rating);
    void changeStatus(FeedbackStatus status);
}
