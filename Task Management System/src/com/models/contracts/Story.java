package com.models.contracts;

import com.models.enums.PriorityType;
import com.models.enums.StorySize;
import com.models.enums.StoryStatus;

public interface Story extends Assignable{
    StorySize getSize();
    StoryStatus getStatus();
    void changeSize(StorySize size);
    void changeStatus(StoryStatus status);
}
