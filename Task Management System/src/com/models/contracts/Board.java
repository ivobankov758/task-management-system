package com.models.contracts;

import java.util.List;

public interface Board extends Nameable{
 List<Task> getTasks();
 List<String> getHistory();
 void addTask (Task task);
 void addHistory(String history);




}
