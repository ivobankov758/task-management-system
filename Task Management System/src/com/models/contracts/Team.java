package com.models.contracts;

import java.util.List;

public interface Team extends Nameable {
    List<Member> getMembers();

    List<Board> getBoards();

    List<String> getHistory();

    void addHistory(String description);

    void addBoard(Board board);

    void addMember(Member member);
}
