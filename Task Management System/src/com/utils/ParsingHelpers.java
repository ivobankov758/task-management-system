package com.utils;

import com.exeptions.InvalidUserInputException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ParsingHelpers<T> {
    private static final String INVALID_NUMBER_FIELD_MESSAGE = "Invalid value for %s. Should be a number.";
    private static final String INVALID_BOOLEAN_FIELD_MESSAGE = "Invalid value for %s. Should be one of 'true' or 'false'.";
    private static final String INVALID_LIST_FIELD_MESSAGE = "Invalid value for %s. Should be a list of %s.";

    public static double tryParseDouble(String valueToParse, String parameterName) {
        try {
            return Double.parseDouble(valueToParse);
        } catch (NumberFormatException e) {
            throw new InvalidUserInputException(String.format(INVALID_NUMBER_FIELD_MESSAGE, parameterName));
        }
    }

//    public static List<String> tryParseList(String valueToParse, String parameterName) {
//        try {
//            return Arrays.asList(valueToParse);
//        } catch (IllegalArgumentException e) {
//            throw new InvalidUserInputException(String.format(INVALID_LIST_FIELD_MESSAGE, valueToParse));
//        }
//    }

    public static int tryParseInteger(String valueToParse, String parameterName) {
        try {
            return Integer.parseInt(valueToParse);
        } catch (NumberFormatException e) {
            throw new InvalidUserInputException(String.format(INVALID_NUMBER_FIELD_MESSAGE, parameterName));
        }
    }

    public static boolean tryParseBoolean(String valueToParse, String parameterName) {
        if (!valueToParse.equalsIgnoreCase("true") &&
                !valueToParse.equalsIgnoreCase("false")) {
            throw new InvalidUserInputException(String.format(INVALID_BOOLEAN_FIELD_MESSAGE, parameterName));
        }

        return Boolean.parseBoolean(valueToParse);
    }

    public static <E extends Enum<E>> E tryParseEnum(String valueToParse, Class<E> type, String errorMessage) {
        try {
            return Enum.valueOf(type, valueToParse.toUpperCase());
        }
        catch (IllegalArgumentException e) {
            throw new InvalidUserInputException(String.format(errorMessage, valueToParse));
        }
    }

    public static String formatTime(LocalDateTime dateTime){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");

        return dateTime.format(formatter);
    }

}
