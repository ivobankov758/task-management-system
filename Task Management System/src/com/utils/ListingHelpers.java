package com.utils;

import com.models.contracts.Nameable;
import com.models.contracts.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.commands.CommandsConstants.JOIN_DELIMITER;

public class ListingHelpers {

//    public static String memberToString(List<Member> members) {
//        return elementsToString(members);
//    }

    public static <T extends Nameable> String elementsToString(List<T> elements) {
        List<String> result = new ArrayList<>();
        for (T element : elements) {
            result.add(element.getName());
        }

        return String.join( System.lineSeparator(), result).trim();
    }

    public static <T extends Task> String TasksToString(List<T> elements) {
        if (elements.isEmpty()){
            return "There are no tasks!";
        }

        List<String> result = new ArrayList<>();
        for (T element : elements) {
            result.add(element.toString());
        }

        return String.join(JOIN_DELIMITER + System.lineSeparator(), result).trim();
    }

    public static <T extends Task> String ImportantInfoToString(List<T> elements) {
        if (elements.isEmpty()){
            return "There are no tasks!";
        }

        List<String> result = new ArrayList<>();
        for (T element : elements) {
            result.add(element.printImportantInfo());
        }

        return String.join(JOIN_DELIMITER + System.lineSeparator(), result).trim();
    }

    public static String printHistory (List<String> logs){

        if (logs.isEmpty()){
            return "---NO HISTORY---\n";
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n~~~~~~~~~~~\n");

        String result = logs
                .stream()
                .map((log -> String.format("%s%n",log)))
                .collect(Collectors.joining(""));

        stringBuilder.append(result);
        stringBuilder.append("~~~~~~~~~~~\n");

        return stringBuilder.toString();
    }
}
