package com.commands;

public final class CommandsConstants {
    private CommandsConstants() {
    }

    public static final String TASK_CREATED = "Task with ID %d was created.";

    public final static String COMMENT_ADDED = "%s added comment successfully!";

    public final static String TASK_ASSIGNED = "Task with ID %d was assigned to %s";

    public final static String TASK_UNASSIGNED = "Task with ID %d was unassigned from %s";

    public final static String FIELD_CHANGED = "The %s of task ID %d was changed from %s to %s.";

    public final static String PERSON_ADDED = "Person %s was added to team %s.";

    public static final String JOIN_DELIMITER = "****************";

}
