package com.commands.securtiy;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;

import java.util.List;

public class LogoutCommand extends BaseCommand {

    public final static String MEMBER_LOGGED_OUT = "You logged out!";

    public LogoutCommand(TaskManagementRepository vehicleDealershipRepository) {
        super(vehicleDealershipRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        getTaskManagementRepository().logout();
        return MEMBER_LOGGED_OUT;
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }
}
