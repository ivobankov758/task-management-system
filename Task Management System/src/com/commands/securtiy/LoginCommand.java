package com.commands.securtiy;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.ValidationHelpers;

import java.util.List;

public class LoginCommand extends BaseCommand {

    private final static String MEMBER_LOGGED_IN = "Member %s successfully logged in!";
    public final static String MEMBER_LOGGED_IN_ALREADY = "Member %s is logged in! Please log out first!";
    private final static String WRONG_MEMBER_OR_TEAM = "Member and team does not match!";

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    private String memberName;
    private String memberTeam;

    public LoginCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        throwIfMemberLoggedIn();
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        Member memberFound = getTaskManagementRepository().findMemberByName(memberName);
        Team teamFound = getTaskManagementRepository().findTeamByName(memberTeam);

        if (!teamFound.getMembers().contains(memberFound)) {
            throw new IllegalArgumentException(WRONG_MEMBER_OR_TEAM);
        }

        getTaskManagementRepository().login(memberFound);
        return String.format(MEMBER_LOGGED_IN, memberName);
    }

    private void parseParameters(List<String> parameters) {
        memberName = parameters.get(0);
        memberTeam = parameters.get(1);
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }

    private void throwIfMemberLoggedIn() {
        if (getTaskManagementRepository().hasLoggedInMember()) {
            throw new IllegalArgumentException(
                    String.format(MEMBER_LOGGED_IN_ALREADY, getTaskManagementRepository().getLoggedInMember().getName())
            );
        }
    }

}
