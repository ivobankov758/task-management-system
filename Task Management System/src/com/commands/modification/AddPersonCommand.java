package com.commands.modification;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.PERSON_ADDED;

public class AddPersonCommand extends BaseCommand {


    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    public AddPersonCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    private String addPersonToTeam(String memberName, String teamName) {
        Team team = getTaskManagementRepository().findTeamByName(teamName);

        Member memberToAdd = getTaskManagementRepository().findMemberByName(memberName);
        List<Member> addToList = team.getMembers();

        if (addToList.contains(memberToAdd)) {
            validateName(memberName, String.format(MEMBER_ALREADY_EXIST, memberName));
        }

        team.addMember(memberToAdd);
        memberToAdd.setTeam(team);
        memberToAdd.addHistory(String.format(PERSON_ADDED, memberName, teamName));
        team.addHistory(String.format(PERSON_ADDED, memberName, teamName));

        return String.format(PERSON_ADDED, memberName, teamName);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String memberName = parameters.get(0);
        String teamName = parameters.get(1);

        return addPersonToTeam(memberName, teamName);
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
