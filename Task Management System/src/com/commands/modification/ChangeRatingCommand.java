package com.commands.modification;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Feedback;
import com.models.contracts.Team;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.FIELD_CHANGED;

public class ChangeRatingCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private int newRating;

    private Board board;
    private Feedback feedback;

    public ChangeRatingCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        int oldRating = feedback.getRating();
        feedback.changeRating(newRating);

        return String.format(FIELD_CHANGED, "rating", feedback.getId(), oldRating, feedback.getRating());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters){
        String boardName = parameters.get(0);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(1), "task id");
        newRating = ParsingHelpers.tryParseInteger(parameters.get(2), "rating");

        board = getTaskManagementRepository().findBoardByName(boardName);
        feedback = getTaskManagementRepository()
                .findTaskById(taskId, getTaskManagementRepository().getFeedbacks());
    }

    private void validateAction(){
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();
        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }

        if (!board.getTasks().contains(feedback)){
            throw  new IllegalArgumentException(String.format(INVALID_TASK, feedback.getId(), board.getName()));
        }
    }
}
