package com.commands.modification;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.*;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.COMMENT_ADDED;

public class AddCommentCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private Board board;
    private Task task;
    private Comment comment;
    private Member author;

    public AddCommentCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        task.addComment(comment);

        return String.format(COMMENT_ADDED, author.getName());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters){
        String boardName = parameters.get(0);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(1), "task id");
        String content = parameters.get(2);

        author = getTaskManagementRepository().getLoggedInMember();
        board = getTaskManagementRepository().findBoardByName(boardName);
        task = getTaskManagementRepository().findTaskById(taskId, getTaskManagementRepository().getTasks());
        comment = getTaskManagementRepository().createComment(author.getName(), content);
    }

    private void validateAction() {
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();

        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }

        if (!board.getTasks().contains(task)){
            throw  new IllegalArgumentException(String.format(INVALID_TASK, task.getId(), board.getName()));
        }
    }
}
