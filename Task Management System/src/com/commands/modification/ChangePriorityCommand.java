package com.commands.modification;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Assignable;
import com.models.contracts.Board;
import com.models.contracts.Team;
import com.models.enums.PriorityType;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.FIELD_CHANGED;

public class ChangePriorityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private PriorityType newPriority;

    private Board board;
    private Assignable task;

    public ChangePriorityCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        PriorityType oldPriority = task.getPriority();
        task.changePriority(newPriority);

        return String.format(FIELD_CHANGED, "priority", task.getId(), oldPriority, task.getPriority());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters){
        String boardName = parameters.get(0);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(1), "task id");
        newPriority = ParsingHelpers.tryParseEnum(parameters.get(2), PriorityType.class,"priority type");

        board = getTaskManagementRepository().findBoardByName(boardName);
        task = getTaskManagementRepository()
                .findTaskById(taskId, getTaskManagementRepository().getAssignableTasks());
    }

    private void validateAction(){
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();
        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }

        if (!board.getTasks().contains(task)){
            throw new IllegalArgumentException(String.format(INVALID_TASK, task.getId(), board.getName()));
        }
    }
}
