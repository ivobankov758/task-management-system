package com.commands.modification;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.*;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.TASK_ASSIGNED;

public class AssignTaskCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private Board board;
    private Assignable task;
    private Member assignee;

    public AssignTaskCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        task.setAssignee(assignee);
        assignee.addTask(task);
        assignee.addHistory(String.format(TASK_ASSIGNED, task.getId(), assignee.getName()));

        return String.format(TASK_ASSIGNED, task.getId(), assignee.getName());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters){
        String boardName = parameters.get(0);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(1), "task id");
        String assigneeName = parameters.get(2);


        task = getTaskManagementRepository()
                .findTaskById(taskId, getTaskManagementRepository().getAssignableTasks());
        board = getTaskManagementRepository().findBoardByName(boardName);
        assignee = getTaskManagementRepository().findMemberByName(assigneeName);
    }

    private void validateAction(){
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();
        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }

        if (!board.getTasks().contains(task)){
            throw  new IllegalArgumentException(String.format(INVALID_TASK, task.getId(), board.getName()));
        }
    }
}
