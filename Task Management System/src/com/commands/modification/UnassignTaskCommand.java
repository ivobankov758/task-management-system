package com.commands.modification;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Assignable;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.TASK_UNASSIGNED;

public class UnassignTaskCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private Assignable task;
    private Member formerAssignee;

    public UnassignTaskCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        task.setAssignee(Assignable.DEFAULT_ASSIGNEE);
        formerAssignee.removeTask(task);
        formerAssignee.addHistory(String.format(TASK_UNASSIGNED, task.getId(), formerAssignee.getName()));

        return String.format(TASK_UNASSIGNED, task.getId(), formerAssignee.getName());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters){
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(0), "task id");

        task = getTaskManagementRepository()
                .findTaskById(taskId, getTaskManagementRepository().getAssignableTasks());
        formerAssignee = task.getAssignee();
    }

    private void validateAction(){
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();
        Team formerAssigneeTeam = formerAssignee.getTeam();
        if (loggedMemberTeam == null || !loggedMemberTeam.equals(formerAssigneeTeam)){
            throw new IllegalArgumentException(TEAMS_NOT_MATCHING);
        }
    }
}
