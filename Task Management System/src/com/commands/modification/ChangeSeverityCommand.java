package com.commands.modification;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Bug;
import com.models.contracts.Team;
import com.models.enums.BugSeverity;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.FIELD_CHANGED;

public class ChangeSeverityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private BugSeverity newSeverity;

    private Board board;
    private Bug bug;

    public ChangeSeverityCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        BugSeverity oldSeverity = bug.getSeverity();
        bug.changeSeverity(newSeverity);

        return String.format(FIELD_CHANGED, "severity", bug.getId(), oldSeverity, bug.getSeverity());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters){
        String boardName = parameters.get(0);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(1), "task id");
        newSeverity = ParsingHelpers.tryParseEnum(parameters.get(2), BugSeverity.class, "severity");

        board = getTaskManagementRepository().findBoardByName(boardName);
        bug = getTaskManagementRepository()
                .findTaskById(taskId, getTaskManagementRepository().getBugs());
    }

    private void validateAction(){
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();
        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }

        if (!board.getTasks().contains(bug)){
            throw  new IllegalArgumentException(String.format(INVALID_TASK, bug.getId(), board.getName()));
        }
    }
}
