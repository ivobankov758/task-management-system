package com.commands.modification;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.*;
import com.models.enums.BugStatus;
import com.models.enums.FeedbackStatus;
import com.models.enums.StoryStatus;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.FIELD_CHANGED;

public class ChangeStatusCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private String newStatus;

    private Board board;
    private Task task;

    public ChangeStatusCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {

        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        switch (task.getType()){
            case BUG:
                Bug bug = (Bug) task;
                BugStatus oldStatus = bug.getStatus();
                bug.changeStatus(ParsingHelpers.tryParseEnum(newStatus, BugStatus.class, "status"));
                return String.format(FIELD_CHANGED, "status", task.getId(), oldStatus, bug.getStatus());
            case STORY:
                Story story = (Story) task;
                StoryStatus oldStatus2 = story.getStatus();
                story.changeStatus(ParsingHelpers.tryParseEnum(newStatus, StoryStatus.class, "status"));
                return String.format(FIELD_CHANGED, "status", task.getId(), oldStatus2, story.getStatus());
            case FEEDBACK:
                Feedback feedback = (Feedback) task;
                FeedbackStatus oldStatus3 = feedback.getStatus();
                feedback.changeStatus(ParsingHelpers.tryParseEnum(newStatus, FeedbackStatus.class, "status"));
                return String.format(FIELD_CHANGED, "status", task.getId(), oldStatus3, feedback.getStatus());
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters){
        String boardName = parameters.get(0);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(1), "task id");
        newStatus = parameters.get(2);

        board = getTaskManagementRepository().findBoardByName(boardName);
        task = getTaskManagementRepository()
                .findTaskById(taskId, getTaskManagementRepository().getTasks());
    }

    private void validateAction(){
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();

        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }

        if (!board.getTasks().contains(task)){
            throw  new IllegalArgumentException(String.format(INVALID_TASK, task.getId(), board.getName()));
        }
    }
}
