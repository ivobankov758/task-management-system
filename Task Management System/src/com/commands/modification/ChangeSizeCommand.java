package com.commands.modification;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Story;
import com.models.contracts.Team;
import com.models.enums.StorySize;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.FIELD_CHANGED;

public class ChangeSizeCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    private StorySize newSize;

    private Board board;
    private Story story;

    public ChangeSizeCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        StorySize oldSize = story.getSize();
        story.changeSize(newSize);

        return String.format(FIELD_CHANGED, "size", story.getId(), oldSize, story.getSize());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters){
        String boardName = parameters.get(0);
        int taskId = ParsingHelpers.tryParseInteger(parameters.get(1), "task id");
        newSize = ParsingHelpers.tryParseEnum(parameters.get(2), StorySize.class, "size");

        board = getTaskManagementRepository().findBoardByName(boardName);
        story = getTaskManagementRepository()
                .findTaskById(taskId, getTaskManagementRepository().getStories());
    }

    private void validateAction(){
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();

        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }

        if (!board.getTasks().contains(story)){
            throw  new IllegalArgumentException(String.format(INVALID_TASK, story.getId(), board.getName()));
        }
    }
}
