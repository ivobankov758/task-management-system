package com.commands.creation;

import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Feedback;
import com.models.contracts.Team;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.TASK_CREATED;

public class CreateFeedbackCommand extends BaseCommand {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 4;

    private Board board;
    private Feedback feedback;

    public CreateFeedbackCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        board.addTask(feedback);
        board.addHistory(String.format(TASK_CREATED, feedback.getId()));

        return String.format(TASK_CREATED, feedback.getId());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters) {
        String title = parameters.get(0);
        String description = parameters.get(1);
        int rating = ParsingHelpers.tryParseInteger(parameters.get(2), "rating");
        String boardName = parameters.get(3);

        board = getTaskManagementRepository().findBoardByName(boardName);
        feedback = getTaskManagementRepository().createFeedback(title, description, rating);
    }

    private void validateAction() {
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();
        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }
    }
}
