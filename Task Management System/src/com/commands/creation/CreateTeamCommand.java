package com.commands.creation;

import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Team;
import com.utils.ValidationHelpers;

import java.util.List;

public class CreateTeamCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public CreateTeamCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String teamName = parameters.get(0);
        Team team = getTaskManagementRepository().createTeam(teamName);

        return String.format(TEAM_CREATED_MESSAGE, team.getName());
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }


}
