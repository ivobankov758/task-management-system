package com.commands.creation;

import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Team;
import com.utils.ValidationHelpers;

import java.util.List;

public class CreateBoardCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;

    String boardName;
    String teamName;
    Board board;
    Team team;

    public CreateBoardCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        boardName = parameters.get(0);
        teamName = parameters.get(1);

        team = getTaskManagementRepository().findTeamByName(teamName);
        board = getTaskManagementRepository().createBoard(boardName);

        validateAction();

        team.addBoard(board);
        team.addHistory(String.format(BOARD_CREATED_MESSAGE, board.getName(),teamName));

        return String.format(BOARD_CREATED_MESSAGE, board.getName(),teamName );
    }

    private void validateAction() {
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();
        if (!loggedMemberTeam.equals(team)){
            throw new IllegalArgumentException(TEAMS_NOT_MATCHING);
        }

    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }


}
