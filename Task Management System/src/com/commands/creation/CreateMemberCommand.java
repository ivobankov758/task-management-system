package com.commands.creation;

import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.utils.ValidationHelpers;

import java.util.List;


public class CreateMemberCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    public CreateMemberCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        String memberName = parameters.get(0);
        Member member = getTaskManagementRepository().createMember(memberName);
        member.addHistory(String.format(MEMBER_CREATED_MESSAGE, member.getName()));

        return String.format(MEMBER_CREATED_MESSAGE, member.getName());
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }

}
