package com.commands.creation;

import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Bug;
import com.models.contracts.Team;
import com.models.enums.BugSeverity;
import com.models.enums.PriorityType;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.commands.CommandsConstants.TASK_CREATED;

public class CreateBugCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 6;

    private Board board;
    private Bug bug;

    public CreateBugCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        board.addTask(bug);
        board.addHistory(String.format(TASK_CREATED, bug.getId()));

        return String.format(TASK_CREATED, bug.getId());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters) {
        String title = parameters.get(0);
        String description = parameters.get(1);
        PriorityType priority = ParsingHelpers.tryParseEnum(parameters.get(2), PriorityType.class, "priority type");
        BugSeverity severity = ParsingHelpers.tryParseEnum(parameters.get(3), BugSeverity.class, "severity");
        List<String> steps = new ArrayList<>(Arrays.asList(parameters.get(4).split(";")));
        String boardName = parameters.get(5);

        board = getTaskManagementRepository().findBoardByName(boardName);
        bug = getTaskManagementRepository().createBug(title, description, priority, severity, steps);
    }

    private void validateAction() {
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();
        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }
    }

}
