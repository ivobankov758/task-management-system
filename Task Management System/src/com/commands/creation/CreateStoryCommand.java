package com.commands.creation;

import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Story;
import com.models.contracts.Team;
import com.models.enums.*;
import com.utils.ParsingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

import static com.commands.CommandsConstants.TASK_CREATED;

public class CreateStoryCommand extends BaseCommand {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;

    private Board board;
    private Story story;

    public CreateStoryCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    public String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        validateAction();

        board.addTask(story);
        board.addHistory(String.format(TASK_CREATED, story.getId()));

        return String.format(TASK_CREATED, story.getId());
    }

    @Override
    protected boolean requiresLogin() {
        return true;
    }

    private void parseParameters(List<String> parameters) {
        String title = parameters.get(0);
        String description = parameters.get(1);
        PriorityType priority = ParsingHelpers.tryParseEnum(parameters.get(2), PriorityType.class, "priority type");
        StorySize size = ParsingHelpers.tryParseEnum(parameters.get(3), StorySize.class, "size");
        String boardName = parameters.get(4);

        board = getTaskManagementRepository().findBoardByName(boardName);
        story = getTaskManagementRepository().createStory(title, description, priority, size);
    }

    private void validateAction() {
        Team loggedMemberTeam = getTaskManagementRepository().getLoggedInMember().getTeam();
        if (loggedMemberTeam == null || !loggedMemberTeam.getBoards().contains(board)){
            throw new IllegalArgumentException(String.format(TEAMS_NOT_MATCHING_BOARD, board.getName()));
        }
    }

}
