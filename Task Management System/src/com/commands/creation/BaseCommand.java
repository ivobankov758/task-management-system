package com.commands.creation;

import com.commands.contracts.Command;
import com.core.contracts.TaskManagementRepository;

import java.util.List;

public abstract class BaseCommand implements Command {

    public static final String TEAMS_NOT_MATCHING_BOARD = "Your team does not have a board %s!";
    public static final String TEAMS_NOT_MATCHING = "Teams do not match!";
    private final static String MEMBER_NOT_LOGGED = "You are not logged in! Please login first!";
    public static final String INVALID_TASK = "Task with ID %d does not belong to board %s!";
    public static final String BOARD_CREATED_MESSAGE = "Board with name %s was created in team %s.";
    public static final String MEMBER_CREATED_MESSAGE = "Member with name %s was created.";
    public final static String MEMBER_ALREADY_EXIST = "Member with name %s already exist. Choose a different name!";
    public static final String TEAM_CREATED_MESSAGE = "Team with name %s was created.";

    private static TaskManagementRepository taskManagementRepository;

    public BaseCommand(TaskManagementRepository taskManagementRepository) {
        BaseCommand.taskManagementRepository = taskManagementRepository;
    }

    protected static TaskManagementRepository getTaskManagementRepository() {
        return taskManagementRepository;
    }

    public static void validateName(String Name, String errorMessage) {
        if (getTaskManagementRepository().getMembers().stream().anyMatch(u -> u.getName().equalsIgnoreCase(Name))) {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    @Override
    public String execute(List<String> parameters) {
        if (requiresLogin() && !taskManagementRepository.hasLoggedInMember()) {
            throw new IllegalArgumentException(MEMBER_NOT_LOGGED);
        }

        return executeCommand(parameters);
    }

    protected abstract String executeCommand(List<String> parameters);

    protected abstract boolean requiresLogin();
}
