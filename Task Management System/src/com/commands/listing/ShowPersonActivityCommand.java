package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.utils.ListingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

public class ShowPersonActivityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private Member member;

    public ShowPersonActivityCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return ListingHelpers.printHistory(member.getHistory());
    }

    private void parseParameters(List<String> parameters){
        String memberName = parameters.get(0);
        member = getTaskManagementRepository().findMemberByName(memberName);
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
