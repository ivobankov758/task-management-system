package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.utils.ListingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

public class ShowBoardActivityCommand extends BaseCommand {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private Board board;

    public ShowBoardActivityCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return ListingHelpers.printHistory(board.getHistory());
    }

    private void parseParameters(List<String> parameters) {
        String boardName = parameters.get(0);
        board = getTaskManagementRepository().findBoardByName(boardName);
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
