package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Task;
import com.utils.ListingHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListAllStories extends BaseCommand {

    public ListAllStories(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        String assigneeName = parameters.get(0);
        return ListingHelpers.TasksToString(getTaskManagementRepository().getStories().stream()
                .filter(storie -> storie.getAssignee().getName().equals(assigneeName))
                .sorted(Comparator.comparing(Task::getTitle))
                .collect(Collectors.toList()));
    }


    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
