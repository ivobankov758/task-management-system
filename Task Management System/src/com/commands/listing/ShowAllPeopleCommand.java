package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.utils.ListingHelpers;

import java.util.List;

public class ShowAllPeopleCommand extends BaseCommand {

    public ShowAllPeopleCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    private String showAllPeople() {
        return ListingHelpers.elementsToString(getTaskManagementRepository().getMembers());
    }


    @Override
    protected String executeCommand(List<String> parameters) {
        return showAllPeople();
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
