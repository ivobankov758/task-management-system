package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Task;
import com.utils.ListingHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListAllBugs extends BaseCommand {

    public ListAllBugs(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);

    }

    @Override
    protected String executeCommand(List<String> parameters) {

        if (parameters.size() == 1) {
            String assigneeName = parameters.get(0);

            return ListingHelpers.TasksToString(getTaskManagementRepository().getBugs().stream()
                    .filter(bug -> bug.getAssignee().getName().equals(assigneeName))
                    .sorted(Comparator.comparing(Task::getTitle))
                    .collect(Collectors.toList()));
        }

        return ListingHelpers.TasksToString(
                getTaskManagementRepository().getAssignableTasks().stream()
                        .filter(bug -> !bug.getAssignee().getName().equals("unassigned"))
                        .sorted(Comparator.comparing(Task::getTitle))
                        .collect(Collectors.toList()));

    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }


}
