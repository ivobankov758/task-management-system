package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Task;
import com.utils.ListingHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListAssignedTasksCommand extends BaseCommand {

    public ListAssignedTasksCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {

        if (parameters.size() == 1){
            String targetAssignee = parameters.get(0);

            return ListingHelpers.TasksToString(
                    getTaskManagementRepository().getAssignableTasks().stream()
                            .filter(task -> task.getAssignee().getName().equals(targetAssignee))
                            .sorted(Comparator.comparing(Task::getTitle))
                            .collect(Collectors.toList()));
        }

        return ListingHelpers.TasksToString(
                getTaskManagementRepository().getAssignableTasks().stream()
                        .filter(task -> !task.getAssignee().getName().equals("unassigned"))
                        .sorted(Comparator.comparing(Task::getTitle))
                        .collect(Collectors.toList()));


    }


    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
