package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.utils.ListingHelpers;

import java.util.List;

public class ShowAllTeamsCommand extends BaseCommand {

    public ShowAllTeamsCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    private String showAllTeams() {
        return ListingHelpers.elementsToString(getTaskManagementRepository().getTeam());
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        return showAllTeams();
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
