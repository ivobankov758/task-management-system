package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Task;
import com.utils.ListingHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListAllTasksCommand extends BaseCommand {

    public ListAllTasksCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {

        if(parameters.size() == 1){
            String targetTitle = parameters.get(0);

            return ListingHelpers.ImportantInfoToString(
                    getTaskManagementRepository().getTasks().stream()
                            .filter(task -> task.getTitle().equals(targetTitle))
                            .sorted(Comparator.comparing(Task::getTitle))
                            .collect(Collectors.toList()));
        }

        return ListingHelpers.ImportantInfoToString(
                getTaskManagementRepository().getTasks().stream()
                        .sorted(Comparator.comparing(Task::getTitle))
                        .collect(Collectors.toList()));

    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
