package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Team;
import com.utils.ListingHelpers;
import com.utils.ValidationHelpers;

import java.util.List;

public class ShowTeamActivityCommand extends BaseCommand {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private Team team;

    public ShowTeamActivityCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        ValidationHelpers.validateArgumentsCount(parameters, EXPECTED_NUMBER_OF_ARGUMENTS);

        parseParameters(parameters);

        return ListingHelpers.printHistory(team.getHistory());
    }

    private void parseParameters(List<String> parameters){
        String teamName = parameters.get(0);
        team = getTaskManagementRepository().findTeamByName(teamName);
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
