package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.utils.ListingHelpers;

import java.util.List;

public class ShowAllTeamBoardsCommand extends BaseCommand {

    public ShowAllTeamBoardsCommand(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }


    private String showAllTeamBoards(String teamName) {

        return ListingHelpers.elementsToString(getTaskManagementRepository().findTeamByName(teamName).getBoards());

    }

    @Override
    protected String executeCommand(List<String> parameters) {
        String teamName = parameters.get(0);
        return showAllTeamBoards(teamName);
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
