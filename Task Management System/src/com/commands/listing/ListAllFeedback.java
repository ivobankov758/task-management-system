package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Task;
import com.models.enums.FeedbackStatus;
import com.utils.ListingHelpers;
import com.utils.ParsingHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ListAllFeedback extends BaseCommand {


    public ListAllFeedback(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {

        FeedbackStatus status = ParsingHelpers.tryParseEnum(parameters.get(0), FeedbackStatus.class, "invalid status");

        return ListingHelpers.TasksToString(getTaskManagementRepository().getFeedbacks().stream()
                .filter(feedback -> feedback.getStatus().equals(status))
                .sorted(Comparator.comparing(Task::getTitle))
                .collect(Collectors.toList()));
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }
}
