package com.commands.listing;

import com.commands.creation.BaseCommand;
import com.core.contracts.TaskManagementRepository;
import com.utils.ListingHelpers;

import java.util.List;

public class ShowAllTeamMembers extends BaseCommand {


    public ShowAllTeamMembers(TaskManagementRepository taskManagementRepository) {
        super(taskManagementRepository);
    }

    @Override
    protected String executeCommand(List<String> parameters) {
        String teamName = parameters.get(0);
        return showAllTeamMembers(teamName);
    }

    @Override
    protected boolean requiresLogin() {
        return false;
    }

    private String showAllTeamMembers(String teamName){
       return ListingHelpers.elementsToString(getTaskManagementRepository().findTeamByName(teamName).getMembers());
    }
}
