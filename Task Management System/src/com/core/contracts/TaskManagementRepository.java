package com.core.contracts;

import com.models.contracts.*;
import com.models.enums.*;

import java.util.List;

public interface TaskManagementRepository {
    List<Member> getMembers();

    List<Task> getTasks();

    List<Assignable> getAssignableTasks();

    List<Bug> getBugs();

    List<Feedback> getFeedbacks();

    List<Story> getStories();

    List<Board> getBoards();

    List<Team> getTeam();

    Member createMember(String memberName);

    Board createBoard(String boardName);

    Team createTeam(String teamName);

    Bug createBug(String title, String description, PriorityType priority,
                  BugSeverity severity, List<String> steps);

    Story createStory(String title, String description, PriorityType priority,
                      StorySize size);

    Feedback createFeedback(String title, String description, int rating);

    Comment createComment(String author, String content);


    Member findMemberByName(String memberName);

    Board findBoardByName(String boardName);

    Team findTeamByName(String teamName);

    <T extends Identifiable> T findTaskById(int id, List<T> elements);

    <T extends Nameable> T findElementByName(List<T> elements, String name);

    Member getLoggedInMember();

    boolean hasLoggedInMember();

    void login(Member member);

    void logout();
}
