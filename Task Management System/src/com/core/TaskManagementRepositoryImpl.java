package com.core;

import com.core.contracts.TaskManagementRepository;

import com.exeptions.ElementNotFoundException;
import com.exeptions.InvalidUserInputException;
import com.models.*;
import com.models.contracts.*;
import com.models.enums.*;

import java.util.ArrayList;
import java.util.List;

public class TaskManagementRepositoryImpl implements TaskManagementRepository {

    private static final String NO_LOGGED_IN_MEMBER = "There is no logged in member.";
    public static final String INVALID_ID = "There is no element with id %d.";
    public static final String INVALID_NAME = "No record with name %s";

    private Member loggedMember;
    private int nextId;

    private final List<Task> tasks = new ArrayList<>();
    private final List<Assignable> assignableTasks = new ArrayList<>();

    private final List<Bug> bugs = new ArrayList<>();
    private final List<Feedback> feedbacks = new ArrayList<>();
    private final List<Story> stories = new ArrayList<>();

    private final List<Member> allMembers = new ArrayList<>();
    private final List<Board> allBoards = new ArrayList<>();
    private final List<Team> allTeams = new ArrayList<>();

    public TaskManagementRepositoryImpl() {
        nextId = 0;
    }

    @Override
    public List<Member> getMembers() {
        return new ArrayList<>(allMembers);
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    @Override
    public List<Assignable> getAssignableTasks() {
        return new ArrayList<>(assignableTasks);
    }

    @Override
    public List<Bug> getBugs() {
        return new ArrayList<>(bugs);
    }

    @Override
    public List<Feedback> getFeedbacks() {
        return new ArrayList<>(feedbacks);
    }

    @Override
    public List<Story> getStories() {
        return new ArrayList<>(stories);
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(allBoards);
    }

    @Override
    public List<Team> getTeam() {
        return new ArrayList<>(allTeams);
    }

    @Override
    public Member createMember(String memberName) {
        Member member = new MemberImpl(memberName);

        if (!allMembers.contains(member)) {
            this.allMembers.add(member);
        }
        else {
            throw new IllegalArgumentException("This person already exists!");
        }

        return member;
    }

    @Override
    public Board createBoard(String boardName) {

        Board board = new BoardImpl(boardName);

        if (!allBoards.contains(board)) {
            this.allBoards.add(board);
        }
        else {
            throw new IllegalArgumentException("This board already exists!");
        }
        return board;

    }

    @Override
    public Team createTeam(String teamName) {
        Team team = new TeamImpl(teamName);

        if (!allTeams.contains(team)) {
            this.allTeams.add(team);
        }
        else {
            throw new IllegalArgumentException("This team already exists!");
        }
        return team;

    }

    @Override
    public Bug createBug(String title, String description, PriorityType priority,
                         BugSeverity severity, List<String> steps) {
        Bug bug = new BugImpl(++nextId, title, description, priority, severity, steps);
        tasks.add(bug);
        bugs.add(bug);
        assignableTasks.add(bug);
        return bug;
    }

    @Override
    public Story createStory(String title, String description, PriorityType priority, StorySize size) {
        Story story = new StoryImpl(++nextId, title, description, priority, size);
        tasks.add(story);
        stories.add(story);
        assignableTasks.add(story);
        return story;
    }

    @Override
    public Feedback createFeedback(String title, String description, int rating) {
        Feedback feedback = new FeedbackImpl(++nextId, title, description, rating);
        tasks.add(feedback);
        feedbacks.add(feedback);
        return feedback;
    }

    @Override
    public Comment createComment(String author, String content) {
        return new CommentImpl(author, content);
    }

    @Override
    public Member findMemberByName(String memberName) {
        return findElementByName(getMembers(), memberName);
    }

    @Override
    public Board findBoardByName(String boardName) {
        return findElementByName(getBoards(), boardName);
    }

    @Override
    public Team findTeamByName(String teamName) {
        return findElementByName(getTeam(), teamName);
    }

    @Override
    public <T extends Identifiable> T findTaskById(int id, List<T> elements) {
        T res = elements.stream().filter(element -> element.getId() == id).findAny().orElse(null);
        if (res == null) {
            throw new ElementNotFoundException(String.format(INVALID_ID, id));
        }
        return res;
    }

    @Override
    public <T extends Nameable> T findElementByName(List<T> elements, String name) {

        return elements
                .stream()
                .filter(element -> element.getName().equals(name))
                .findAny()
                .orElseThrow(() -> new ElementNotFoundException(
                        String.format("No %s with name %s",elements.getClass().getSimpleName(), name)));
    }


    @Override
    public Member getLoggedInMember() {
        if (loggedMember == null) {
            throw new IllegalArgumentException(NO_LOGGED_IN_MEMBER);
        }
        return loggedMember;
    }

    @Override
    public boolean hasLoggedInMember() {
        return loggedMember != null;
    }

    @Override
    public void login(Member member) {
        loggedMember = member;
    }

    @Override
    public void logout() {
        loggedMember = null;
    }
}


