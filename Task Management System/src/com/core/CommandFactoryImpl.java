package com.core;

import com.commands.contracts.Command;
import com.commands.creation.*;
import com.commands.enums.CommandType;
import com.commands.listing.*;
import com.commands.modification.*;
import com.commands.securtiy.LoginCommand;
import com.commands.securtiy.LogoutCommand;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.utils.ParsingHelpers;

public class CommandFactoryImpl implements CommandFactory {

    private static final String INVALID_COMMAND = "Invalid command name: %s!";

    @Override
    public Command createCommandFromCommandName(String commandTypeAsString,
                                                TaskManagementRepository taskManagementRepository) {

        CommandType commandType = ParsingHelpers.tryParseEnum(commandTypeAsString, CommandType.class,
                String.format(INVALID_COMMAND, commandTypeAsString));

        switch (commandType) {
            case CREATEBUG:
                return new CreateBugCommand(taskManagementRepository);
            case CREATESTORY:
                return new CreateStoryCommand(taskManagementRepository);
            case CREATEFEEDBACK:
                return new CreateFeedbackCommand(taskManagementRepository);
            case CREATEBOARD:
                return new CreateBoardCommand(taskManagementRepository);
            case CREATEPERSON:
                return new CreateMemberCommand(taskManagementRepository);
            case CREATETEAM:
                return new CreateTeamCommand(taskManagementRepository);
            case ADDPERSON:
                return new AddPersonCommand(taskManagementRepository);
            case ADDCOMMENT:
                return new AddCommentCommand(taskManagementRepository);
            case ASSIGN:
                return new AssignTaskCommand(taskManagementRepository);
            case UNASSIGN:
                return new UnassignTaskCommand(taskManagementRepository);
            case CHANGEPRIORITY:
                return new ChangePriorityCommand(taskManagementRepository);
            case CHANGERATING:
                return new ChangeRatingCommand(taskManagementRepository);
            case CHANGESEVERITY:
                return new ChangeSeverityCommand(taskManagementRepository);
            case CHANGESIZE:
                return new ChangeSizeCommand(taskManagementRepository);
            case CHANGESTATUS:
                return new ChangeStatusCommand(taskManagementRepository);
            case LISTALLTASKS:
                return new ListAllTasksCommand(taskManagementRepository);
            case LISTASSIGNEDTASKS:
                return new ListAssignedTasksCommand(taskManagementRepository);
            case LISTALLBUGS:
                return new ListAllBugs(taskManagementRepository);
            case LISTALLSTORIES:
                return new ListAllStories(taskManagementRepository);
            case LISTALLFEEDBACKS:
                return new ListAllFeedback(taskManagementRepository);
            case SHOWALLPEOPLE:
                return new ShowAllPeopleCommand(taskManagementRepository);
            case SHOWALLTEAMS:
                return new ShowAllTeamsCommand(taskManagementRepository);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembers(taskManagementRepository);
            case SHOWALLTEAMBOARDS:
                return new ShowAllTeamBoardsCommand(taskManagementRepository);
            case SHOWTEAMACTIVITY:
                return new ShowTeamActivityCommand(taskManagementRepository);
            case SHOWPERSONACTIVITY:
                return new ShowPersonActivityCommand(taskManagementRepository);
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivityCommand(taskManagementRepository);
            case LOGIN:
                return new LoginCommand(taskManagementRepository);
            case LOGOUT:
                return new LogoutCommand(taskManagementRepository);
            default:
                throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandTypeAsString));
        }
    }
}
