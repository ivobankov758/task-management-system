package com;

import com.core.TaskManagementEngineImpl;
import com.core.contracts.TaskManagementEngine;

import java.util.ArrayList;
import java.util.List;

public class Startup {

    public static void main(String[] args) {
        TaskManagementEngine taskManagementEngine = new TaskManagementEngineImpl();
        taskManagementEngine.start();
    }
}
