package com.lisitng.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateBoardCommand;
import com.commands.listing.ShowAllTeamBoardsCommand;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.ListingHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.utils.TestData.TeamData.VALID_MEMBER_NAME;

public class ShowAllTeamBoardsCommandTests {
    private Command command;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new ShowAllTeamBoardsCommand(taskManagementRepository);

        Member testMember = taskManagementRepository.createMember(VALID_MEMBER_NAME);
        Team team1 = taskManagementRepository.createTeam("team1");

        team1.addMember(testMember);
        testMember.setTeam(team1);

        taskManagementRepository.login(testMember);
    }

    @Test
    public void execute_Should_ShowAllTeamBoards_When_PassedValidInput() {
        List<String> parameters = List.of("team1");
        Command helpCommand = new CreateBoardCommand(taskManagementRepository);
        helpCommand.execute(List.of("board1", "team1"));
        helpCommand.execute(List.of("board2", "team1"));

        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()->Assertions.assertEquals(ListingHelpers.elementsToString(taskManagementRepository.getBoards()),
                        String.format("board1%n" +
                                "board2"))
        );

    }

}
