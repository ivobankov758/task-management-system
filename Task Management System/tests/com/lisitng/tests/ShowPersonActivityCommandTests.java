package com.lisitng.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateMemberCommand;
import com.commands.listing.ShowPersonActivityCommand;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.utils.ListingHelpers;
import com.utils.ParsingHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.TeamData.VALID_MEMBER_NAME;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class ShowPersonActivityCommandTests {

    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private Member testMember;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new ShowPersonActivityCommand(taskManagementRepository);
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(ShowPersonActivityCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ShowPersonActivity_When_PassedValidInput(){
        List<String> parameters = List.of(VALID_TEAM_NAME);

        Command helpCommand = new CreateMemberCommand(taskManagementRepository);
        helpCommand.execute(List.of(VALID_MEMBER_NAME));
        testMember = taskManagementRepository.findMemberByName(VALID_MEMBER_NAME);

        Assertions.assertAll(
                ()-> Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()-> Assertions.assertEquals(ListingHelpers.printHistory(testMember.getHistory()),
                        String.format("\n~~~~~~~~~~~\n" +
                                        "%s - Member with name %s was created.%n" +
                                        "~~~~~~~~~~~\n",
                                ParsingHelpers.formatTime(LocalDateTime.now()), testMember.getName()))
        );
    }
}
