package com.lisitng.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateBugCommand;
import com.commands.listing.ShowBoardActivityCommand;
import com.commands.listing.ShowTeamActivityCommand;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.ListingHelpers;
import com.utils.ParsingHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.BugData.*;
import static com.utils.TestData.TaskData.VALID_DESCRIPTION;
import static com.utils.TestData.TaskData.VALID_TITLE;
import static com.utils.TestData.TeamData.*;

public class ShowBoardActivityCommandTests {

    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private Board testBoard;
    private Member testMember;
    private Team testTeam;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new ShowBoardActivityCommand(taskManagementRepository);

        testMember =  taskManagementRepository.createMember(VALID_MEMBER_NAME);
        testTeam =  taskManagementRepository.createTeam(VALID_TEAM_NAME);
        testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(ShowBoardActivityCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ShowBoardActivity_When_PassedValidInput(){
        testTeam.addMember(testMember);
        testMember.setTeam(testTeam);
        testTeam.addBoard(testBoard);

        taskManagementRepository.login(testMember);

        List<String> parameters = List.of(VALID_TEAM_NAME);

        Command helpCommand = new CreateBugCommand(taskManagementRepository);
        helpCommand.execute(List.of(VALID_TITLE, VALID_DESCRIPTION, String.valueOf(VALID_PRIORITY),
                String.valueOf(VALID_SEVERITY), String.valueOf(VALID_STEPS), testBoard.getName()));

        Assertions.assertAll(
                ()-> Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()-> Assertions.assertEquals(ListingHelpers.printHistory(testBoard.getHistory()),
                        String.format("\n~~~~~~~~~~~\n" +
                                        "%s - Task with ID 1 was created.%n" +
                                        "~~~~~~~~~~~\n",
                                ParsingHelpers.formatTime(LocalDateTime.now())))
        );
    }
}
