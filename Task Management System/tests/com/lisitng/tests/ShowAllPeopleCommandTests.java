package com.lisitng.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateBoardCommand;
import com.commands.listing.ShowAllPeopleCommand;
import com.commands.listing.ShowBoardActivityCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.utils.ListingHelpers;
import com.utils.ParsingHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class ShowAllPeopleCommandTests {
    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private CommandFactory commandFactory;
    private Member person1;
    private Member person2;
    private Member person3;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new ShowAllPeopleCommand(taskManagementRepository);
        this.commandFactory = new CommandFactoryImpl();

        person1 = taskManagementRepository.createMember("Person1");
        person2 = taskManagementRepository.createMember("Person2");
        person3 = taskManagementRepository.createMember("Person3");

    }

    @Test
    public void execute_Should_ShowAllPerson_When_PassedValidInput() {
        List<String> parameters = new ArrayList<>();

        Assertions.assertAll(
                () -> Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()->Assertions.assertEquals(ListingHelpers.elementsToString(taskManagementRepository.getMembers()),
                                String.format("Person1%n" +
                                        "Person2%n" +
                                        "Person3"))
        );

    }

}
