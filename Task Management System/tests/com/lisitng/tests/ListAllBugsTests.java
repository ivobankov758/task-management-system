package com.lisitng.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateBoardCommand;
import com.commands.listing.ListAllBugs;
import com.commands.modification.AddPersonCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Task;
import com.utils.ListingHelpers;
import com.utils.ParsingHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.utils.TestData.TeamData.VALID_MEMBER_NAME;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class ListAllBugsTests {
    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private CommandFactory commandFactory;
    private String testAssigneeName;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new ListAllBugs(taskManagementRepository);
        this.commandFactory = new CommandFactoryImpl();
    }


}
