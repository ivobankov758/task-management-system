package com.lisitng.tests;

import com.commands.contracts.Command;
import com.commands.listing.ShowTeamActivityCommand;
import com.commands.modification.AddPersonCommand;
import com.commands.modification.ChangePriorityCommand;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.ListingHelpers;
import com.utils.ParsingHelpers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.BoardData.VALID_BOARD_NAME;
import static com.utils.TestData.TeamData.VALID_MEMBER_NAME;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class ShowTeamActivityCommandTests {

    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private Team testTeam;
    private Member testMember;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new ShowTeamActivityCommand(taskManagementRepository);

        testMember = taskManagementRepository.createMember(VALID_MEMBER_NAME);
        testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(ShowTeamActivityCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ShowTeamActivity_When_PassedValidInput(){
        List<String> parameters = List.of(VALID_TEAM_NAME);

        Command helpCommand = new AddPersonCommand(taskManagementRepository);
        helpCommand.execute(List.of(testMember.getName(),testTeam.getName()));

        Assertions.assertAll(
                ()-> Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()-> Assertions.assertEquals(ListingHelpers.printHistory(testTeam.getHistory()),
                        String.format("\n~~~~~~~~~~~\n" +
                        "%s - Person %s was added to team %s.%n" +
                                "~~~~~~~~~~~\n",
                                ParsingHelpers.formatTime(LocalDateTime.now()), testMember.getName(), testTeam.getName()))
        );
    }
}
