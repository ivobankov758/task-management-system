package com.modification.tests;

import com.commands.contracts.Command;
import com.commands.modification.ChangeSeverityCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.*;
import com.models.enums.BugSeverity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.BoardData.VALID_BOARD_NAME;
import static com.utils.TestData.BugData.*;
import static com.utils.TestData.BugData.VALID_ASSIGNEE;
import static com.utils.TestData.TaskData.VALID_DESCRIPTION;
import static com.utils.TestData.TaskData.VALID_TITLE;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class ChangeSeverityCommandTests {
    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private CommandFactory commandFactory;

    private Team testTeam;
    private Board testBoard;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new ChangeSeverityCommand(taskManagementRepository);
        this.commandFactory = new CommandFactoryImpl();


        taskManagementRepository.createBug(VALID_TITLE, VALID_DESCRIPTION, VALID_PRIORITY, VALID_SEVERITY, VALID_STEPS)
                .setAssignee(VALID_ASSIGNEE);

        testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);
        testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);

        testTeam.addBoard(taskManagementRepository.findBoardByName(VALID_BOARD_NAME));
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(ChangeSeverityCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ModifyingOthersTeamBoard(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        taskManagementRepository.login(testMember);
        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "minor");

        Assertions.assertThrows(IllegalArgumentException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ModifyingOthersTeamTask(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        taskManagementRepository.login(testMember);
        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "minor");

        Assertions.assertThrows(IllegalArgumentException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_ChangePriority_When_PassedValidInput(){
        Command createCommand = commandFactory.createCommandFromCommandName("changeSeverity",
                taskManagementRepository);

        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        testBoard.addTask(taskManagementRepository.findTaskById(1, taskManagementRepository.getTasks()));
        taskManagementRepository.login(testMember);

        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "minor");

        Bug testTask = taskManagementRepository.findTaskById(1, taskManagementRepository.getBugs());

        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()->Assertions.assertEquals(testTask.getSeverity(), BugSeverity.MINOR)
        );
    }
}
