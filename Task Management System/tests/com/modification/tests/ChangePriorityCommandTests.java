package com.modification.tests;

import com.commands.contracts.Command;
import com.commands.modification.ChangePriorityCommand;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.*;
import com.models.enums.PriorityType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.BoardData.VALID_BOARD_NAME;
import static com.utils.TestData.BugData.*;
import static com.utils.TestData.BugData.VALID_ASSIGNEE;
import static com.utils.TestData.TaskData.VALID_DESCRIPTION;
import static com.utils.TestData.TaskData.VALID_TITLE;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class ChangePriorityCommandTests {

    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private Team testTeam;
    private Board testBoard;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new ChangePriorityCommand(taskManagementRepository);

        taskManagementRepository.createBug(VALID_TITLE, VALID_DESCRIPTION, VALID_PRIORITY, VALID_SEVERITY, VALID_STEPS)
                .setAssignee(VALID_ASSIGNEE);

        testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);
        testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);

        testTeam.addBoard(taskManagementRepository.findBoardByName(VALID_BOARD_NAME));
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(ChangePriorityCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ModifyingOthersTeamBoard(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        taskManagementRepository.login(testMember);
        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "high");

        Assertions.assertThrows(IllegalArgumentException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ModifyingOthersTeamTask(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        taskManagementRepository.login(testMember);
        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "high");

        Assertions.assertThrows(IllegalArgumentException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_ChangePriority_When_PassedValidInput(){

        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        testBoard.addTask(taskManagementRepository.findTaskById(1, taskManagementRepository.getTasks()));
        taskManagementRepository.login(testMember);

        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "high");

        Assignable testTask = taskManagementRepository.findTaskById(1, taskManagementRepository.getAssignableTasks());

        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()->Assertions.assertEquals(testTask.getPriority(), PriorityType.HIGH)
        );
    }
}
