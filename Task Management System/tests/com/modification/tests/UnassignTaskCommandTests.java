package com.modification.tests;

import com.commands.contracts.Command;
import com.commands.modification.AssignTaskCommand;
import com.commands.modification.UnassignTaskCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Assignable;
import com.models.contracts.Board;
import com.models.contracts.Member;
import com.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.commands.CommandsConstants.TASK_ASSIGNED;
import static com.utils.TestData.BoardData.*;
import static com.utils.TestData.BugData.*;
import static com.utils.TestData.MemberData.*;
import static com.utils.TestData.TaskData.*;

public class UnassignTaskCommandTests {
    private TaskManagementRepository taskManagementRepository;
    private CommandFactory commandFactory;

    private Command command;
    private Team testTeam;
    private Board testBoard;
    private Member formerMember;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new UnassignTaskCommand(taskManagementRepository);
        this.commandFactory = new CommandFactoryImpl();


        formerMember = taskManagementRepository.createMember(VALID_MEMBER_NAME);
        testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);
        testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);

        Assignable testTask = taskManagementRepository.createBug(VALID_TITLE, VALID_DESCRIPTION, VALID_PRIORITY,
                VALID_SEVERITY, VALID_STEPS);

        testTeam.addBoard(taskManagementRepository.findBoardByName(VALID_BOARD_NAME));
        testBoard.addTask(taskManagementRepository.findTaskById(1, taskManagementRepository.getAssignableTasks()));
        formerMember.setTeam(testTeam);
        taskManagementRepository.login(formerMember);

        testTask.setAssignee(formerMember);
        formerMember.addTask(testTask);
        formerMember.addHistory(String.format(TASK_ASSIGNED, testTask.getId(), formerMember.getName()));

    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(UnassignTaskCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void executeCommand_Should_UnassignTask_When_InputIsValid() {
        Command createCommand = commandFactory.createCommandFromCommandName("unassign",
                taskManagementRepository);

        List<String> params = List.of( "1");

        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(params)),
                () -> Assertions.assertEquals("unassigned", taskManagementRepository.findTaskById(1, taskManagementRepository.getAssignableTasks()).getAssignee().getName())
        );
    }


}
