package com.modification.tests;

import com.commands.contracts.Command;
import com.commands.modification.ChangeSeverityCommand;
import com.commands.modification.ChangeStatusCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.exeptions.InvalidUserInputException;
import com.models.BugImpl;
import com.models.FeedbackImpl;
import com.models.StoryImpl;
import com.models.contracts.*;
import com.models.enums.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.BoardData.VALID_BOARD_NAME;
import static com.utils.TestData.BugData.*;
import static com.utils.TestData.BugData.VALID_ASSIGNEE;
import static com.utils.TestData.MemberData.VALID_MEMBER_NAME;
import static com.utils.TestData.TaskData.VALID_DESCRIPTION;
import static com.utils.TestData.TaskData.VALID_TITLE;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class ChangeStatusCommandTests {
    private Command command;
    private CommandFactory commandFactory;
    private TaskManagementRepository taskManagementRepository;

    private Team testTeam;
    private Board testBoard;
    private Task testTask;
    private Member testMember;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new ChangeStatusCommand(taskManagementRepository);
        this.commandFactory = new CommandFactoryImpl();


        taskManagementRepository.createBug(VALID_TITLE, VALID_DESCRIPTION, VALID_PRIORITY, VALID_SEVERITY, VALID_STEPS)
                .setAssignee(VALID_ASSIGNEE);
        taskManagementRepository.createStory(VALID_TITLE,VALID_DESCRIPTION,PriorityType.LOW,StorySize.SMALL);
        taskManagementRepository.createFeedback(VALID_TITLE,VALID_DESCRIPTION,15);

        testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);
        testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);

        testTeam.addBoard(taskManagementRepository.findBoardByName(VALID_BOARD_NAME));

    }
    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(ChangeStatusCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ModifyingOthersTeamBoard(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        taskManagementRepository.login(testMember);

        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "Active");

        Assertions.assertThrows(IllegalArgumentException.class, ()->command.execute(parameters));
    }
    @Test
    public void execute_Should_ThrowException_When_ModifyingOthersTeamTask(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        taskManagementRepository.login(testMember);

        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "Fixed");

        Assertions.assertThrows(IllegalArgumentException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_InvalidStatus(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        testBoard.addTask(taskManagementRepository.findTaskById(1, taskManagementRepository.getTasks()));
        taskManagementRepository.login(testMember);

        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "NotStatus");

        Assertions.assertThrows(InvalidUserInputException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_ChangeBugStatus_when_validParameters(){

        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        testBoard.addTask(taskManagementRepository.findTaskById(1, taskManagementRepository.getTasks()));
        taskManagementRepository.login(testMember);

        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "Fixed");
        Bug testTask = taskManagementRepository.findTaskById(1, taskManagementRepository.getBugs());


        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()->Assertions.assertEquals(testTask.getStatus(),BugStatus.FIXED)
        );
    }

    @Test
    public void execute_Should_ChangeStoryStatus_when_validParameters(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        taskManagementRepository.login(testMember);
        testBoard.addTask(taskManagementRepository.findTaskById(2, taskManagementRepository.getAssignableTasks()));

        List<String> parameters = List.of(VALID_BOARD_NAME, "2", "Done");
        Story testTask = taskManagementRepository.findTaskById(2, taskManagementRepository.getStories());


        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()->Assertions.assertEquals(testTask.getStatus(), StoryStatus.DONE)
        );
    }
    @Test
    public void execute_Should_ChangeFeedbackStatus_when_validParameters(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        taskManagementRepository.login(testMember);
        testBoard.addTask(taskManagementRepository.findTaskById(3, taskManagementRepository.getTasks()));

        List<String> parameters = List.of(VALID_BOARD_NAME, "3", "Unscheduled");
        Feedback testTask = taskManagementRepository.findTaskById(3, taskManagementRepository.getFeedbacks());

        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()->Assertions.assertEquals(testTask.getStatus(), FeedbackStatus.UNSCHEDULED)
        );
    }
}
