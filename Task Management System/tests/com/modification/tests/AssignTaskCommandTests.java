package com.modification.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateBoardCommand;
import com.commands.creation.CreateStoryCommand;
import com.commands.modification.AssignTaskCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.models.BoardImpl;
import com.models.TeamImpl;
import com.models.contracts.*;
import com.utils.TestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;


import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.BoardData.VALID_BOARD_NAME;
import static com.utils.TestData.BoardData.VALID_TEAM_NAME;
import static com.utils.TestData.BugData.*;
import static com.utils.TestData.MemberData.VALID_MEMBER_NAME;
import static com.utils.TestData.TaskData.VALID_DESCRIPTION;
import static com.utils.TestData.TaskData.VALID_TITLE;

public class AssignTaskCommandTests {

    private TaskManagementRepository taskManagementRepository;
    private Command command;
    private CommandFactory commandFactory;
    private Team testTeam;
    private Board testBoard;
    private Member testMember;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new AssignTaskCommand(taskManagementRepository);
        this.commandFactory = new CommandFactoryImpl();


        testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);
        testTeam = taskManagementRepository.createTeam(TestData.TeamData.VALID_TEAM_NAME);
        testMember = taskManagementRepository.createMember(VALID_MEMBER_NAME);

        testTeam.addBoard(taskManagementRepository.findBoardByName(VALID_BOARD_NAME));

        Assignable testTask = taskManagementRepository.createBug(VALID_TITLE, VALID_DESCRIPTION, VALID_PRIORITY,
                VALID_SEVERITY, VALID_STEPS);

        testBoard.addTask(taskManagementRepository.findTaskById(1, taskManagementRepository.getAssignableTasks()));
        testMember.setTeam(testTeam);
        taskManagementRepository.login(testMember);

    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(AssignTaskCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void executeCommand_Should_AssignTask_When_InputIsValid() {
        Command createCommand = commandFactory.createCommandFromCommandName("assign",
                taskManagementRepository);


        List<String> params = List.of(VALID_BOARD_NAME, "1", VALID_MEMBER_NAME);

        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(params)),
                () -> Assertions.assertEquals(VALID_MEMBER_NAME,
                        taskManagementRepository.findTaskById(1, taskManagementRepository.getAssignableTasks()).getAssignee().getName())
        );
    }


    @Test
    public void validateAction_should_throwException_when_TeamDoesNotContainsBoard(){
        Member testMember = taskManagementRepository.createMember(VALID_MEMBER_NAME);
        Team testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);
        Board testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);
        testBoard.addTask(taskManagementRepository.findTaskById(1, taskManagementRepository.getAssignableTasks()));
        testMember.setTeam(testTeam);
        taskManagementRepository.login(testMember);
        List<String> params = List.of(VALID_BOARD_NAME, "1", VALID_MEMBER_NAME);

        Assertions.assertThrows(IllegalArgumentException.class,()->command.execute(params));
    }
//TODO throw new Illegal....

//    @Test
//    public void execute_should_throwException_when_BoardDoesNotContainsTask() {
//        Member testMember = taskManagementRepository.createMember(VALID_MEMBER_NAME);
//        Team testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);
////        Board testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);
//        testTeam.addBoard(taskManagementRepository.findBoardByName(VALID_BOARD_NAME));
////        testBoard.addTask(taskManagementRepository.findTaskById(1, taskManagementRepository.getAssignableTasks()));
//        testMember.setTeam(testTeam);
//        taskManagementRepository.login(testMember);
//        List<String> params = List.of(VALID_BOARD_NAME, "1", VALID_MEMBER_NAME);
//
//        Assertions.assertThrows(IllegalArgumentException.class,()->command.execute(params));
//    }
}
