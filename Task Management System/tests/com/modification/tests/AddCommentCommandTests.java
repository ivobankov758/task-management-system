package com.modification.tests;

import com.commands.contracts.Command;
import com.commands.modification.AddCommentCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Board;
import com.models.contracts.Member;
import com.models.contracts.Task;
import com.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.BoardData.*;
import static com.utils.TestData.BugData.*;
import static com.utils.TestData.TaskData.*;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class AddCommentCommandTests {
    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private CommandFactory commandFactory;

    private Team testTeam;
    private Board testBoard;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.commandFactory = new CommandFactoryImpl();

        this.command = new AddCommentCommand(taskManagementRepository);

        taskManagementRepository.createBug(VALID_TITLE, VALID_DESCRIPTION, VALID_PRIORITY, VALID_SEVERITY, VALID_STEPS)
                .setAssignee(VALID_ASSIGNEE);

        testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);
        testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);

        testTeam.addBoard(taskManagementRepository.findBoardByName(VALID_BOARD_NAME));
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(AddCommentCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ModifyingOthersTeamBoard(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        taskManagementRepository.login(testMember);
        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "some comment");

        Assertions.assertThrows(IllegalArgumentException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_ModifyingOthersTeamTask(){
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testMember.setTeam(testTeam);
        testTeam.addMember(testMember);
        taskManagementRepository.login(testMember);
        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "some comment");

        Assertions.assertThrows(IllegalArgumentException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_AddNewComment_When_PassedValidInput(){
        Command createCommand = commandFactory.createCommandFromCommandName("addComment",
                taskManagementRepository);

        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());
        testBoard.addTask(taskManagementRepository.findTaskById(1, taskManagementRepository.getTasks()));
        testMember.setTeam(testTeam);
        taskManagementRepository.login(testMember);

        List<String> parameters = List.of(VALID_BOARD_NAME, "1", "some comment");

        Task task = taskManagementRepository.findTaskById(1, taskManagementRepository.getTasks());

        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                () -> Assertions.assertFalse(task.getComments().isEmpty())
        );
    }
}
