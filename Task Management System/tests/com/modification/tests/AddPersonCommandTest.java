package com.modification.tests;

import com.commands.contracts.Command;
import com.commands.modification.AddPersonCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.exeptions.ElementNotFoundException;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.TestData;
import com.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class AddPersonCommandTest {
    private List<String> parameters;
    private TaskManagementRepository repository;
    private CommandFactory commandFactory;

    private AddPersonCommand command;

    @BeforeEach
    public void before() {
        parameters = new ArrayList<>();
        repository = new TaskManagementRepositoryImpl();
        command = new AddPersonCommand(repository);
        this.commandFactory = new CommandFactoryImpl();

    }

    @Test
    public void executeCommand_should_addPersonToTeam_when_validParameters() {
        Command createCommand = commandFactory.createCommandFromCommandName("addPerson",
                repository);
        String teamName = TestData.TeamData.VALID_TEAM_NAME;
        String memberName = TestData.MemberData.VALID_MEMBER_NAME;
        repository.createTeam(teamName);
        repository.createMember(memberName);

        parameters.add(teamName);
        parameters.add(memberName);

        command.execute(parameters);

        Team team = repository.findTeamByName(teamName);
        Member member = repository.findMemberByName(memberName);
        Assertions.assertTrue(team.getMembers().contains(member));
    }

    @Test
    public void executeCommand_should_throwException_when_missingParameters() {
        parameters = TestUtilities.initializeListWithSize(AddPersonCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void executeCommand_should_throwException_when_memberNotExist() {
        String teamName = TestData.TeamData.VALID_TEAM_NAME;
        repository.createTeam(teamName);
        repository.createMember(TestData.MemberData.VALID_MEMBER_NAME);

        parameters.add(teamName);
        parameters.add("invalid-member-name");

        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(parameters));
    }
//TODO if already exist?
    //TODO valid name?

}
