package com.security.tests;

import com.commands.contracts.Command;
import com.commands.securtiy.LogoutCommand;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.TeamData.VALID_MEMBER_NAME;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class LogoutCommandTests {

    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private Member testMember;
    private Team testTeam;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new LogoutCommand(taskManagementRepository);

        testMember = taskManagementRepository.createMember(VALID_MEMBER_NAME);
        testTeam =  taskManagementRepository.createTeam(VALID_TEAM_NAME);
    }

    @Test
    public void execute_Should_Login_When_PassedValidInput(){
        testTeam.addMember(testMember);
        testMember.setTeam(testTeam);
        taskManagementRepository.login(testMember);

        List<String> parameters = new ArrayList<>();

        Assertions.assertAll(
                ()->Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()->Assertions.assertThrows(IllegalArgumentException.class,
                        ()->taskManagementRepository.getLoggedInMember())
        );
    }
}
