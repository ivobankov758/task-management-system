package com.security.tests;

import com.commands.contracts.Command;
import com.commands.securtiy.LoginCommand;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.utils.TestData.TeamData.VALID_MEMBER_NAME;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class LoginCommandTests {

    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private Member testMember;
    private Team testTeam;

    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new LoginCommand(taskManagementRepository);

        testMember = taskManagementRepository.createMember(VALID_MEMBER_NAME);
        testTeam =  taskManagementRepository.createTeam(VALID_TEAM_NAME);
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(LoginCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_TrowException_When_AlreadyLogged(){
        testTeam.addMember(testMember);
        testMember.setTeam(testTeam);
        taskManagementRepository.login(testMember);

        List<String> parameters = List.of(VALID_MEMBER_NAME, VALID_TEAM_NAME);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_TrowException_When_TeamAndMember_doNot_Match(){
        Team testTeam2 = taskManagementRepository.createTeam("Some other team");
        testTeam2.addMember(testMember);
        testMember.setTeam(testTeam2);

        List<String> parameters = List.of(VALID_MEMBER_NAME, VALID_TEAM_NAME);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_Login_When_PassedValidInput(){
        testTeam.addMember(testMember);
        testMember.setTeam(testTeam);

        List<String> parameters = List.of(VALID_MEMBER_NAME, VALID_TEAM_NAME);

        Assertions.assertAll(
                ()->Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                ()->Assertions.assertEquals(VALID_MEMBER_NAME, taskManagementRepository.getLoggedInMember().getName())
        );
    }
}
