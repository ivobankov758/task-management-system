package com.models.tests;

import com.models.BugImpl;
import com.models.FeedbackImpl;
import com.models.contracts.Bug;
import com.models.contracts.Feedback;
import com.models.contracts.Task;
import com.models.enums.BugSeverity;
import com.models.enums.BugStatus;
import com.models.enums.FeedbackStatus;
import com.models.enums.PriorityType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

public class FeedbackImplTests {
    private FeedbackImpl feedback;

    @BeforeEach
    private void before(){
        feedback = new FeedbackImpl(2,"Valid Title" ,"Valid description", 20);
    }

    @Test
    public void FeedbackImpl_ShouldImplementBugInterface() {
        // Assert
        Assertions.assertTrue(feedback instanceof Feedback);
    }

    @Test
    public void FeedbackImpl_ShouldImplementTaskInterface() {
        // Assert
        Assertions.assertTrue(feedback instanceof Task);
    }

    @Test
    public void constructor_should_createFeedback_when_argumentsAreValid() {
        Assertions.assertEquals(20, feedback.getRating());
    }

    @Test
    public void changeRating_should_throwException_when_settingSameRating(){
        Assertions.assertThrows(IllegalArgumentException.class, ()->feedback.changeRating(20));
    }

    @Test
    public void changeRating_should_setNewRating(){
        feedback.changeRating(30);
        Assertions.assertEquals(30, feedback.getRating());
    }

    @Test
    public void changeStatus_should_throwException_when_settingSameStatus() {
        Assertions.assertThrows(IllegalArgumentException.class, ()->feedback.changeStatus(FeedbackStatus.NEW));
    }

    @Test
    public void changeStatus_should_setNewStatus(){
        feedback.changeStatus(FeedbackStatus.DONE);
        Assertions.assertEquals(FeedbackStatus.DONE, feedback.getStatus());
    }
}
