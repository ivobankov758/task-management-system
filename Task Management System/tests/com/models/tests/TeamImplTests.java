package com.models.tests;

import com.models.BoardImpl;
import com.models.MemberImpl;
import com.models.TeamImpl;
import com.models.contracts.Board;
import com.models.contracts.Member;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.utils.TestData.TeamData.*;


import java.util.List;

public class TeamImplTests {
    private List<Board> board;
    private List<Member> member;
    private TeamImpl team;
    private Board testBoard;
    private Member testMember;

    @BeforeEach
    public void before() {
        testBoard = new BoardImpl(VALID_BOARD_NAME);
        testMember = new MemberImpl(VALID_MEMBER_NAME);
        team = new TeamImpl(VALID_TEAM_NAME);
    }

    @Test
    public void constructor_should_createTeam_when_argumentsAreValid() {

        Assertions.assertAll(
                () -> Assertions.assertEquals(VALID_TEAM_NAME, team.getName()));

    }

    @Test
    public void constructor_should_throwException_when_nameIsInvalid() {
        String invalidName = "n".repeat(TeamImpl.TEAM_NAME_MIN_LEN - 1);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new TeamImpl(invalidName));
    }

    @Test
    public void constructor_should_throwException_when_nameShorterThanExpected() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new TeamImpl("s"));
    }

    @Test
    public void getHistory_should_returnCopyOfCollection() {
        team.addHistory("Some event");
        team.getHistory().clear();

        Assertions.assertEquals(1, team.getHistory().size());
    }

//    @Test
//    public void addBoard_should_UpdateSize_When_SingleElement() {
//        List<Board> testList = new ArrayList<>();
//        testList.add(testBoard);
//        Assertions.assertEquals(1,testList.size());
//
//    }
//
//    @Test
//    public void addMember_should_UpdateSize_When_SingleElement() {
//        List<Member> testList = new ArrayList<>();
//        testList.add(testMember);
//        Assertions.assertEquals(1,testList.size());
//    }
}
