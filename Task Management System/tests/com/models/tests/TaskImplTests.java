package com.models.tests;

import com.models.BugImpl;
import com.models.CommentImpl;
import com.models.contracts.Comment;
import com.models.contracts.Member;
import com.models.contracts.Task;
import com.models.enums.BugSeverity;
import com.models.enums.PriorityType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.utils.TestData.BugData.VALID_ASSIGNEE;
import static com.utils.TestData.TaskData.*;

public class TaskImplTests {
    private Task task;

    @BeforeEach
    private void before(){
        task = new BugImpl(2,"Valid Title" ,"broken function", PriorityType.MEDIUM,
                BugSeverity.MAJOR, List.of("start the program", "try to close it"));
    }

    @Test
    public void constructor_should_createTask_when_argumentsAreValid() {
        Assertions.assertAll(
                () -> Assertions.assertEquals("Valid Title", task.getTitle()),
                () -> Assertions.assertEquals("broken function", task.getDescription()),
                () -> Assertions.assertNotNull(task.getComments()),
                () -> Assertions.assertNotNull(task.getHistory())
        );
    }

    @Test
    public void constructor_should_throwException_when_TitleTooShort() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(2,"x".repeat(9) ,"broken function", PriorityType.MEDIUM,
                        BugSeverity.MAJOR, List.of("start the program", "try to close it")));
    }

    @Test
    public void constructor_should_throwException_when_TitleTooLong() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(2,"x".repeat(51) ,"broken function", PriorityType.MEDIUM,
                        BugSeverity.MAJOR, List.of("start the program", "try to close it")));
    }

    @Test
    public void constructor_should_throwException_when_descriptionTooShort() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(2,"ValidTitle" ,"x".repeat(9), PriorityType.MEDIUM,
                        BugSeverity.MAJOR, List.of("start the program", "try to close it")));
    }

    @Test
    public void constructor_should_throwException_when_descriptionTooLong() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(2,"ValidTitle" ,"x".repeat(501), PriorityType.MEDIUM,
                        BugSeverity.MAJOR, List.of("start the program", "try to close it")));
    }

    @Test
    public void addComment_ShouldAddNewComment(){
        Comment comment = new CommentImpl("Author","Valid Content");
        task.addComment(comment);
        Assertions.assertEquals(1,task.getComments().size());
    }

    @Test
    public void historyLogger_shouldAddLog_to_history(){
        task.historyLogger(VALID_LOG);
        Assertions.assertEquals(VALID_LOG, task.getHistory().get(0));
    }

    @Test
    public void getComments_should_returnCopyOfCollection() {
        task.addComment(new CommentImpl("Author", "Valid content"));
        task.getComments().clear();

        Assertions.assertEquals(1, task.getComments().size());
    }

    @Test
    public void getHistory_should_returnCopyOfCollection() {
        task.historyLogger("Some event");
        task.getHistory().clear();

        Assertions.assertEquals(1, task.getHistory().size());
    }
}
