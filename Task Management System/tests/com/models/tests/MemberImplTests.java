package com.models.tests;

import com.models.BoardImpl;
import com.models.BugImpl;
import com.models.MemberImpl;
import com.models.contracts.Assignable;
import com.models.contracts.Board;
import com.models.contracts.Member;
import com.models.contracts.Task;
import com.models.enums.BugSeverity;
import com.models.enums.BugStatus;
import com.models.enums.PriorityType;

import static com.utils.TestData.BugData.*;
import static com.utils.TestData.MemberData.*;
import static com.utils.TestData.TaskData.VALID_DESCRIPTION;
import static com.utils.TestData.TaskData.VALID_TITLE;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MemberImplTests {

    private MemberImpl member;
    private Assignable testTask;

    @BeforeEach
    public void before() {
        testTask = new BugImpl(2, VALID_TITLE,VALID_DESCRIPTION, VALID_PRIORITY, VALID_SEVERITY, VALID_STEPS);
        member = new MemberImpl(VALID_MEMBER_NAME);

    }

    @Test
    public void constructor_should_createMember_when_argumentsAreValid() {
        Member member = new MemberImpl(VALID_MEMBER_NAME);

        Assertions.assertAll(
                () -> Assertions.assertEquals(VALID_MEMBER_NAME, member.getName())
        );
    }

    @Test
    public void constructor_should_throwException_when_nameIsInvalid() {
        String invalidName = "n".repeat(MemberImpl.MEMBER_NAME_MIN_LEN - 1);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new MemberImpl(invalidName));
    }

    @Test
    public void constructor_should_throwException_when_nameShorterThanExpected() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new MemberImpl("s"));
    }
//    @Test
//    public void addTask_should_UpdateSize_When_SingleElement() {
//        List<Assignable> testList = new ArrayList<>();
//        testList.add(testTask);
//        Assertions.assertEquals(1,testList.size());
//    }
}
