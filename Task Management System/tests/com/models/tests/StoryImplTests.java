package com.models.tests;

import com.models.MemberImpl;
import com.models.StoryImpl;
import com.models.contracts.Member;
import com.models.contracts.Story;
import com.models.contracts.Task;
import com.models.enums.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.utils.TestData.BugData.VALID_ASSIGNEE;

public class StoryImplTests {
    private StoryImpl story;

    @BeforeEach
    private void before(){
        story = new StoryImpl(2,"Valid Title" ,"Valid description", PriorityType.MEDIUM,
                StorySize.LARGE);
    }

    @Test
    public void StoryImpl_ShouldImplementStoryInterface() {
        // Assert
        Assertions.assertTrue(story instanceof Story);
    }

    @Test
    public void StoryImpl_ShouldImplementTaskInterface() {
        // Assert
        Assertions.assertTrue(story instanceof Task);
    }

    @Test
    public void constructor_should_createStory_when_argumentsAreValid() {
        Assertions.assertAll(
                () -> Assertions.assertEquals(PriorityType.MEDIUM, story.getPriority()),
                () -> Assertions.assertEquals(StorySize.LARGE, story.getSize())
        );
    }

    @Test
    public void changePriority_should_throwException_when_settingSamePriority() {
        Assertions.assertThrows(IllegalArgumentException.class, ()->story.changePriority(PriorityType.MEDIUM));
    }

    @Test
    public void changePriority_should_setNewPriority(){
        story.changePriority(PriorityType.HIGH);
        Assertions.assertEquals(PriorityType.HIGH, story.getPriority());
    }

    @Test
    public void changeAssignee_should_throwException_when_settingSameAssignee() {
        story.setAssignee(VALID_ASSIGNEE);
        Assertions.assertThrows(IllegalArgumentException.class, ()->story.setAssignee(VALID_ASSIGNEE));
    }

    @Test
    public void changeAssignee_should_setNewAssignee() {
        Member newAssignee = new MemberImpl( "Valid Name");
        story.setAssignee(newAssignee);
        Assertions.assertEquals("Valid Name", story.getAssignee().getName());
    }

    @Test
    public void changeStatus_should_throwException_when_settingSameStatus() {
        Assertions.assertThrows(IllegalArgumentException.class, ()->story.changeStatus(StoryStatus.NOT_DONE));
    }

    @Test
    public void changeStatus_should_setNewStatus(){
        story.changeStatus(StoryStatus.DONE);
        Assertions.assertEquals(StoryStatus.DONE, story.getStatus());
    }

    @Test
    public void changeSize_should_throwException_when_settingSameSize() {
        Assertions.assertThrows(IllegalArgumentException.class, ()->story.changeSize(StorySize.LARGE));
    }

    @Test
    public void changeSize_should_setNewSize(){
        story.changeSize(StorySize.MEDIUM);
        Assertions.assertEquals(StorySize.MEDIUM, story.getSize());
    }
}
