package com.models.tests;

import com.models.BoardImpl;

import static com.utils.TestData.BoardData.*;

import com.utils.TestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class BoardImplTests {



    private BoardImpl board;

    @BeforeEach
    public void before() {


        board = new BoardImpl( TestData.BoardData.VALID_BOARD_NAME);

    }

    @Test
    public void constructor_should_createBoard_when_argumentsAreValid() {
        BoardImpl board = new BoardImpl(TestData.BoardData.VALID_BOARD_NAME);

        Assertions.assertAll(
                () -> Assertions.assertEquals(VALID_BOARD_NAME, board.getName())
        );
    }

//    @Test
//    public void BoardImpl_ShouldImplementBoardInterface() {
//        // Assert
//        Assertions.assertTrue(board instanceof Board);
//    }

//    @Test
//    public void constructor_should_createBoard_when_argumentsAreValid(){
//        String boardName=TestData.BoardData.VALID_NAME;
//        BoardImpl board= new BoardImpl("TestBoard", )
//    }

    @Test
    public void constructor_should_throwException_when_nameIsInvalid() {
        String invalidName = "n".repeat(BoardImpl.BOARD_NAME_MIN_LEN - 1);

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BoardImpl( invalidName));
    }

    @Test
    public void constructor_should_throwException_when_nameShorterThanExpected() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new BoardImpl("s"));
    }


}
