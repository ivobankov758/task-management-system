package com.models.tests;

import com.models.BugImpl;
import com.models.MemberImpl;
import com.models.contracts.Bug;
import com.models.contracts.Member;
import com.models.contracts.Task;
import com.models.enums.BugSeverity;
import com.models.enums.BugStatus;
import com.models.enums.PriorityType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static com.utils.TestData.TaskData.*;
import static com.utils.TestData.BugData.*;

public class BugImplTests {
    private BugImpl bug;

    @BeforeEach
    private void before(){
        bug = new BugImpl(2, VALID_TITLE,VALID_DESCRIPTION, VALID_PRIORITY, VALID_SEVERITY, VALID_STEPS);
    }

    @Test
    public void BugImpl_ShouldImplementBugInterface() {
        // Assert
        Assertions.assertTrue(bug instanceof Bug);
    }

    @Test
    public void BugImpl_ShouldImplementTaskInterface() {
        // Assert
        Assertions.assertTrue(bug instanceof Task);
    }

    @Test
    public void constructor_should_createBug_when_argumentsAreValid() {
        Assertions.assertAll(
                () -> Assertions.assertEquals(PriorityType.MEDIUM, bug.getPriority()),
                () -> Assertions.assertEquals(VALID_SEVERITY, bug.getSeverity()),
                () -> Assertions.assertNotNull(bug.getSteps())
        );
    }

    @Test
    public void should_throwException_when_stepsIsEmpty() {
        // Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                new BugImpl(2, VALID_TITLE,VALID_DESCRIPTION, VALID_PRIORITY,
                        VALID_SEVERITY, new ArrayList<>()));
    }

    @Test
    public void changeStatus_should_throwException_when_settingSameStatus() {
        Assertions.assertThrows(IllegalArgumentException.class, ()->bug.changeStatus(VALID_BUG_STATUS));
    }

    @Test
    public void changeStatus_should_setNewStatus(){
        bug.changeStatus(BugStatus.FIXED);
        Assertions.assertEquals(BugStatus.FIXED, bug.getStatus());
    }

    @Test
    public void changeSeverity_should_throwException_when_settingSameSeverity() {
        Assertions.assertThrows(IllegalArgumentException.class, ()->bug.changeSeverity(VALID_SEVERITY));
    }

    @Test
    public void changeSeverity_should_setNewSeverity(){
        bug.changeSeverity(BugSeverity.CRITICAL);
        Assertions.assertEquals(BugSeverity.CRITICAL, bug.getSeverity());
    }

    @Test
    public void changePriority_should_throwException_when_settingSamePriority() {
        Assertions.assertThrows(IllegalArgumentException.class, ()->bug.changePriority(PriorityType.MEDIUM));
    }

    @Test
    public void changePriority_should_setNewPriority(){
        bug.changePriority(PriorityType.HIGH);
        Assertions.assertEquals(PriorityType.HIGH, bug.getPriority());
    }

    @Test
    public void changeAssignee_should_throwException_when_settingSameAssignee() {
        bug.setAssignee(VALID_ASSIGNEE);
        Assertions.assertThrows(IllegalArgumentException.class, ()->bug.setAssignee(VALID_ASSIGNEE));
    }

    @Test
    public void changeAssignee_should_setNewAssignee() {
        Member newAssignee = new MemberImpl( "Valid Name");
        bug.setAssignee(newAssignee);
        Assertions.assertEquals("Valid Name", bug.getAssignee().getName());
    }

    @Test
    public void getSteps_should_returnCopyOfCollection() {
        bug.getSteps().clear();
        Assertions.assertEquals(2, bug.getSteps().size());
    }
}
