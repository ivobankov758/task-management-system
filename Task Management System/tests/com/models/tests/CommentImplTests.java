package com.models.tests;

import com.models.CommentImpl;
import com.models.contracts.Comment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.utils.TestData.BugData.VALID_ASSIGNEE;

public class CommentImplTests {
    @Test
    public void CommentImpl_ShouldImplementCommentInterface() {
        CommentImpl comment = new CommentImpl("author","content");
        Assertions.assertTrue(comment instanceof Comment);
    }

    @Test
    public void should_throwException_when_content_shorterThanExpected() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new CommentImpl("Author","Xx"));
    }

    @Test
    public void should_throwException_when_content_longerThanExpected() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new CommentImpl("Author","x".repeat(201)));
    }

    @Test
    public void getAuthor_should_Return_Author() {
        CommentImpl comment = new CommentImpl(VALID_ASSIGNEE.getName(), "x".repeat(100));

        Assertions.assertEquals(comment.getAuthor(), VALID_ASSIGNEE.getName());
    }

    @Test
    public void getContent_should_Return_Content() {
        CommentImpl comment = new CommentImpl(VALID_ASSIGNEE.getName(), "x".repeat(100));

        Assertions.assertEquals(comment.getContent(), "x".repeat(100));
    }

}
