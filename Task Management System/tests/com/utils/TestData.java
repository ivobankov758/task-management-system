package com.utils;

import com.models.BoardImpl;
import com.models.MemberImpl;
import com.models.TaskImpl;
import com.models.TeamImpl;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.models.contracts.Board;
import com.models.enums.*;

import java.io.LineNumberInputStream;
import java.util.List;
import java.util.Stack;

public class TestData {

    public static class BoardData {

        public static final String VALID_BOARD_NAME = "x".repeat(BoardImpl.BOARD_NAME_MIN_LEN + 1);
        public static final String VALID_TEAM_NAME = "x".repeat(TeamImpl.TEAM_NAME_MIN_LEN + 1);

    }

    public static class MemberData {

        public static final String VALID_MEMBER_NAME = "x".repeat(MemberImpl.MEMBER_NAME_MIN_LEN + 1);
    }


    public static class TeamData {
        public static final String VALID_TEAM_NAME = "x".repeat(TeamImpl.TEAM_NAME_MIN_LEN + 1);
        public static final String VALID_BOARD_NAME = "x".repeat(BoardImpl.BOARD_NAME_MIN_LEN + 1);
        public static final String VALID_MEMBER_NAME = "x".repeat(MemberImpl.MEMBER_NAME_MIN_LEN + 1);

    }

    public static class TaskData{
        public static final String VALID_TITLE = "x".repeat(TaskImpl.MAX_TITLE_LEN);
        public static final String VALID_DESCRIPTION = "x".repeat(TaskImpl.MIN_DESCRIPTION_LEN);
        public static final String VALID_LOG = "x".repeat(20);
    }

    public static class BugData{
        public static final PriorityType VALID_PRIORITY = PriorityType.MEDIUM;
        public static final Member VALID_ASSIGNEE = new MemberImpl( MemberData.VALID_MEMBER_NAME);
        public static final List<String> VALID_STEPS = List.of("x".repeat(10), "x".repeat(10));
        public static final BugSeverity VALID_SEVERITY = BugSeverity.MAJOR;
        public static final BugStatus VALID_BUG_STATUS = BugStatus.ACTIVE;
    }

    public static class FeedbackData{
        public static final int VALID_RATING = 20;
    }

    public static class StoryData{
        public static final PriorityType VALID_PRIORITY = PriorityType.MEDIUM;
        public static final Member VALID_ASSIGNEE = new MemberImpl( MemberData.VALID_MEMBER_NAME);
        public static final StorySize VALID_SIZE = StorySize.LARGE;
    }

}
