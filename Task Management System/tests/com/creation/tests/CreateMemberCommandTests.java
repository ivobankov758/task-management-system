package com.creation.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateBoardCommand;
import com.commands.creation.CreateMemberCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.utils.TestUtilities;
import static com.utils.TestData.MemberData.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static com.commands.creation.CreateMemberCommand.EXPECTED_NUMBER_OF_ARGUMENTS;

public class CreateMemberCommandTests {
    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private CommandFactory commandFactory;


    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new CreateMemberCommand(taskManagementRepository);
        this.commandFactory = new CommandFactoryImpl();
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(CreateMemberCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }
    @Test
    public void createMember_ShouldCreate_WhenInputIsValid() {

        Command createCommand = commandFactory.createCommandFromCommandName("createPerson",
                taskManagementRepository);

        List<String> params = List.of(VALID_MEMBER_NAME);

        createCommand.execute(params);

        Assertions.assertEquals(taskManagementRepository.getMembers().get(0).getName(),VALID_MEMBER_NAME);

    }

}
