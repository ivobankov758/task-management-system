package com.creation.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateMemberCommand;
import com.commands.creation.CreateTeamCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static com.commands.creation.CreateTeamCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class CreateTeamCommandTests {
    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private CommandFactory commandFactory;


    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new CreateTeamCommand(taskManagementRepository);
        this.commandFactory = new CommandFactoryImpl();
    }


    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {

        List<String> parameters = new ArrayList<>(CreateMemberCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);



        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }
    @Test
    public void createMember_ShouldCreate_WhenInputIsValid() {

        Command createCommand = commandFactory.createCommandFromCommandName("createTeam",
                taskManagementRepository);
        List<String> params = List.of(VALID_TEAM_NAME);

        createCommand.execute(params);

        Assertions.assertEquals(taskManagementRepository.getTeam().size(), 1);

    }
}
