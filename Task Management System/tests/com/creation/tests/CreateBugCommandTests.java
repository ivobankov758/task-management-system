package com.creation.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateBugCommand;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.TaskManagementRepository;
import com.exeptions.InvalidUserInputException;
import com.models.BoardImpl;
import com.models.contracts.Board;
import com.models.contracts.Member;
import com.models.contracts.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.commands.creation.CreateBugCommand.*;
import static com.utils.TestData.*;
import static com.utils.TestData.BoardData.VALID_BOARD_NAME;
import static com.utils.TestData.TaskData.*;
import static com.utils.TestData.BugData.*;
import static com.utils.TestData.TeamData.VALID_TEAM_NAME;

public class CreateBugCommandTests {
    private Command command;
    private TaskManagementRepository taskManagementRepository;

    @BeforeEach
    public void before() {
        this.taskManagementRepository= new TaskManagementRepositoryImpl();
        this.command = new CreateBugCommand(taskManagementRepository);

        Board testBoard = taskManagementRepository.createBoard(VALID_BOARD_NAME);
        Team testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);
        Member testMember = taskManagementRepository.createMember(VALID_ASSIGNEE.getName());

        testTeam.addBoard(taskManagementRepository.findBoardByName(VALID_BOARD_NAME));
        testMember.setTeam(testTeam);
        taskManagementRepository.login(testMember);
    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_PriorityNotValidType(){
        List<String> parameters = List.of(VALID_TITLE, VALID_DESCRIPTION, "veryHigh",
                String.valueOf(VALID_SEVERITY), String.valueOf(VALID_STEPS), BoardData.VALID_BOARD_NAME);

        Assertions.assertThrows(InvalidUserInputException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_ThrowException_When_SeverityNotValidType(){
        List<String> parameters = List.of(VALID_TITLE, VALID_DESCRIPTION, String.valueOf(VALID_PRIORITY),
                 "big", String.valueOf(VALID_STEPS), BoardData.VALID_BOARD_NAME);

        Assertions.assertThrows(InvalidUserInputException.class, ()->command.execute(parameters));
    }

    @Test
    public void execute_Should_CreateNewBug_When_PassedValidInput(){
        List<String> parameters = List.of(VALID_TITLE, VALID_DESCRIPTION, String.valueOf(VALID_PRIORITY),
                String.valueOf(VALID_SEVERITY), String.valueOf(VALID_STEPS), BoardData.VALID_BOARD_NAME);

        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(parameters)),
                () -> Assertions.assertFalse(taskManagementRepository.getBugs().isEmpty()),
                () -> Assertions.assertFalse(taskManagementRepository.getTasks().isEmpty())
        );
    }
}
