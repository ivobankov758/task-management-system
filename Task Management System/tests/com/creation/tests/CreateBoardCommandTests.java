package com.creation.tests;

import com.commands.contracts.Command;
import com.commands.creation.CreateBoardCommand;
import com.commands.creation.CreateStoryCommand;
import com.core.CommandFactoryImpl;
import com.core.TaskManagementRepositoryImpl;
import com.core.contracts.CommandFactory;
import com.core.contracts.TaskManagementRepository;
import com.models.contracts.Member;
import com.models.contracts.Team;
import com.utils.TestData;
import com.utils.TestUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;

import static com.commands.creation.CreateBoardCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.utils.TestData.BoardData.*;
import static com.utils.TestData.BugData.VALID_ASSIGNEE;
import static com.utils.TestData.MemberData.VALID_MEMBER_NAME;

public class CreateBoardCommandTests {
    private Command command;
    private TaskManagementRepository taskManagementRepository;
    private CommandFactory commandFactory;


    @BeforeEach
    public void before() {
        this.taskManagementRepository = new TaskManagementRepositoryImpl();
        this.command = new CreateBoardCommand(taskManagementRepository);
        this.commandFactory = new CommandFactoryImpl();
        Member testMember = taskManagementRepository.createMember(VALID_MEMBER_NAME);
        Team testTeam = taskManagementRepository.createTeam(VALID_TEAM_NAME);
        testMember.setTeam(testTeam);
        taskManagementRepository.login(testMember);

    }

    @Test
    public void execute_Should_ThrowException_When_ArgumentsCountDifferentThanExpected() {
        List<String> parameters = new ArrayList<>(CreateBoardCommand.EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(parameters));
    }

    @Test
    public void createBoard_ShouldCreate_WhenInputIsValid() {
        Command createCommand = commandFactory.createCommandFromCommandName("createBoard",
                taskManagementRepository);


        List<String> params = List.of(VALID_BOARD_NAME, VALID_TEAM_NAME);


        Assertions.assertAll(
                Assertions.assertDoesNotThrow(() -> command.execute(params)),
                () -> Assertions.assertFalse(taskManagementRepository.getBoards().isEmpty()),
                () -> Assertions.assertEquals(taskManagementRepository.getBoards().get(0).getName(),VALID_BOARD_NAME)
        );
    }
}
